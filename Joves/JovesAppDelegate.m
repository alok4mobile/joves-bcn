//
//  JovesAppDelegate.m
//  Joves
//
//  Created by Alok Sahay on 11/19/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import "JovesAppDelegate.h"

#import "JovesFirstViewController.h"

#import "JovesConfigViewController.h"

#import "JovesFavoritesViewController.h"

#import "JovesMorePagesViewController.h"

#import "Joves_target.h"

//#import "TestFlight.h"

@implementation JovesAppDelegate

@synthesize data = _data;
@synthesize targetPOI = _targetPOI;
@synthesize gps;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	
	//[TestFlight takeOff:@"5f8c8746-9b90-40b5-9eed-4fc5ae912927"];
	
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
	UIViewController *homeView = [[JovesFirstViewController alloc] initWithNibName:@"JovesFirstViewController" bundle:nil];
    UIViewController *favs = [[JovesFavoritesViewController alloc] initWithNibName:nil bundle:nil];
    UIViewController *config = [[JovesConfigViewController alloc] initWithNibName:@"JovesConfigViewController" bundle:nil];
    UIViewController *moreView = [[JovesMorePagesViewController alloc] initWithNibName:@"JovesMorePagesViewController" bundle:nil];
	
	if([[UINavigationBar class] respondsToSelector:@selector(appearance)]) //iOS >=5.0
	{
		[[UINavigationBar appearance] setTintColor:[Joves_Tools appBlueColor]];
		[[UIBarButtonItem appearance] setTintColor:[UIColor blackColor]];
	}
	
	self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.viewControllers = [NSArray arrayWithObjects:homeView, favs, config, moreView, nil];
	self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:self.tabBarController];
	
	_data = [[Joves_data alloc] init];
	
	[self.window makeKeyAndVisible];
    
	[self startGPS];
	return YES;
	
}

+ (JovesAppDelegate *)sharedAppDelegate {
	return (JovesAppDelegate *) [UIApplication sharedApplication].delegate;
}

-(void)reloadData {
	_data = nil;
	_data = [[Joves_data alloc] init];
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

/*
 // Optional UITabBarControllerDelegate method.
 - (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
 {
 }
 */

/*
 // Optional UITabBarControllerDelegate method.
 - (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
 {
 }
 */

- (void) startGPS {
    self.gps = [[ArgolDevice alloc] init];
    [self.gps startCheckingLocation];
    [self loadTargets];
}

- (void) loadTargets {
    double lat;
    double lon;
    
	int tag = 0;
	
	self.targetPOI = [NSMutableArray new];
	
	
	for (Joves_POI *targetData in _data.wifiPoints) {
		
		lat = [targetData.latitude doubleValue];
		lon = [targetData.longitude doubleValue];
		CLLocation *targetLocation = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
		ArgolTarget *target = [[ArgolTarget alloc] initWithDevice:self.gps targetLocation:targetLocation];
		target.tag = tag;
		[self.gps.targets addObject:target];
		
		
		Joves_target *poi = [[Joves_target alloc] init];
		poi.target = target;
		poi.poi = targetData;
				
		[self.targetPOI addObject:poi];
		
		++tag;
	}
	
	NSLog(@"targets added: %i",tag);
	[self.gps startCheckingTargets];
}

- (void) stopGPS {
    [self.gps stopCheckingTargets];
}
@end

