//
//  JovesMorePagesViewController.m
//  Joves
//
//  Created by Jan Baros on 12/3/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import "JovesMorePagesViewController.h"
#import "JovesMainSectionViewController.h"
#import "JovesTableWithHeaderViewController.h"

@interface JovesMorePagesViewController ()

@end

@implementation JovesMorePagesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem.title = NSLocalizedString(@"Més Info", nil);
        self.tabBarItem.image = [UIImage imageNamed:@"more_info_inactive"];
		
	}
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	_sectionnames = [[NSArray alloc] initWithObjects:@"Estades a l'estranger",@"Habitatge",@"Salut",@"Turisme",@"Mobilitat i Transport",@"Medi Ambient i Sostenibilitat",@"Publicacions", nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:YES];
	[self.navigationController setNavigationBarHidden:NO];
	//self.tabBarController.navigationItem.title = @"More";
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	//int row = indexPath.row;
	
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
		cell.textLabel.text = [_sectionnames objectAtIndex:indexPath.row];
		cell.textLabel.font = [Joves_Tools boldappFontofSize:16];
		cell.textLabel.textColor = [Joves_Tools appBlueColor];
		cell.textLabel.backgroundColor = [UIColor clearColor];
		cell.selectionStyle = UITableViewCellSelectionStyleGray;
	}
	return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 40;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	int row = indexPath.row;
	
	if (row == 4 || row == 5) {
		JovesTableWithHeaderViewController *detailViewController = [[JovesTableWithHeaderViewController alloc] initWithType:[_sectionnames objectAtIndex:indexPath.row]];
		[self.navigationController pushViewController:detailViewController animated:YES];
	}
	else {
	JovesMainSectionViewController *detailViewController = [[JovesMainSectionViewController alloc] initWithType:[_sectionnames objectAtIndex:indexPath.row]];
	[self.navigationController pushViewController:detailViewController animated:YES];
    }
}

@end
