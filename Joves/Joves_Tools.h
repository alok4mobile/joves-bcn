//
//  Joves_Tools.h
//  Joves
//
//  Created by Jan Baros on 11/21/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Joves_Tools : NSObject

// Fonts

+ (UIFont *)appFontofSize:(CGFloat)size;

+ (UIFont *)boldappFontofSize:(CGFloat)size;

// Colors

+ (UIColor *) appBlueColor;

+ (UIColor *) appLightBlueColor;

+ (UIColor *) appGrayColor;

+ (UIColor *) appLightGrayColor;

@end
