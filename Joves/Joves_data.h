//
//  Joves_data.h
//  Joves
//
//  Created by Jan Baros on 11/21/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Joves_data : NSObject <NSXMLParserDelegate>{
	NSMutableDictionary *_jovesData;
	NSMutableArray *_wifiPoints;
	NSMutableArray *_favoritePages;
	NSString *favoritesPath;
	NSString *langString;
}

@property (nonatomic,strong,readonly) NSDictionary *jovesData;
@property (nonatomic,strong,readonly) NSDictionary *cachedData;
@property (nonatomic,strong,readonly) NSArray *agendaRSS;
@property (nonatomic,strong,readonly) NSArray *wifiPoints;
@property (nonatomic,strong,readonly) NSArray *favoritePages;

-(void)addFavoritePage:(NSDictionary *)pageData;
-(void)removeFavoritePage:(NSDictionary *)pageName;
-(BOOL)isPageFavorite:(NSDictionary *)pageName;

@end
