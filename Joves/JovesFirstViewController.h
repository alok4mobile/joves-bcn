//
//  JovesFirstViewController.h
//  Joves
//
//  Created by Alok Sahay on 11/19/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JovesFirstViewController : UIViewController {
	NSArray *_objects;
	UIButton *agendaButton;
	UIButton *formacioButton;
	UIButton *treballButton;
	UIButton *culturaButton;
	UIButton *sportsButton;
	UIButton *coopButton;
	UIButton *particiButton;
	UIButton *wifiButton;
	NSString * language;
}

@end
