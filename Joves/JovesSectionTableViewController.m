//
//  JovesTreballTableViewController.m
//  Joves
//
//  Created by Jan Baros on 11/26/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import "JovesSectionTableViewController.h"
#import "JovesWebSectionViewController.h"

@interface JovesSectionTableViewController ()

@end

@implementation JovesSectionTableViewController

- (id)initWithData:(NSArray*)data withHeader:(NSString*)header andType:(NSString*)type {
	self = [super init];
	if (self) {
		subsectionData = data;
		_header = header;
		sectionType = type;
	}
	return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
     self.clearsSelectionOnViewWillAppear = YES;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    //return subsectionData.count;
	return subsectionData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
	static NSString *title = @"title";
	static NSString *imgURL = @"image";
	static NSString *description = @"description";
	
	NSUInteger row = indexPath.row;
	
	JovesWhiteCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    NSDictionary *_cellData = [subsectionData objectAtIndex:indexPath.row];
        
	cell = [[JovesWhiteCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
	cell.textLabel.text = [_cellData objectForKey:title];
	cell.detailTextLabel.text = [_cellData objectForKey:description];
	NSString *img = [NSString stringWithFormat:@"http://w110.bcn.cat/%@",[_cellData objectForKey:imgURL]];
	cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:img]]];
	
	if (row%2 ==0) {
		cell.contentView.backgroundColor = [UIColor whiteColor];
		return cell;
	}
	
	else
		
	{
		cell.contentView.backgroundColor = [Joves_Tools appLightGrayColor];
		return cell;
	}
    return nil;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 100;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 40;
}
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	
	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 70)];
	headerView.backgroundColor = [Joves_Tools appBlueColor];
	
	UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, headerView.bounds.size.width, 40)];
	headerLabel.backgroundColor = [UIColor clearColor];
	
	headerLabel.text =  _header;
	headerLabel.font = [Joves_Tools appFontofSize:16];
	headerLabel.textColor = [UIColor whiteColor];
	[headerView addSubview:headerLabel];
	return headerView;
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSDictionary *_cellData = [subsectionData objectAtIndex:indexPath.row];
	
	NSString* sectionkey = [self returntype:sectionType];
		
	NSString *key = [_cellData objectForKey:@"title"];
	
	NSDictionary *mainsection = [[JovesAppDelegate sharedAppDelegate].data.cachedData objectForKey:sectionkey];
	id data = [mainsection objectForKey:key];
	
	NSString *subSectionHeader = [[_header stringByAppendingString:@" > "] stringByAppendingString:key];
	
	
	JovesWebSectionViewController *detailView = [[JovesWebSectionViewController alloc] initWithData:data withHeader:subSectionHeader andSection:sectionType];
	[self.navigationController pushViewController:detailView animated:YES];
//
//		
	   
//	
//	NSDictionary *mainsection = [[JovesAppDelegate sharedAppDelegate].data.cachedData objectForKey:sectionkey];
//	
//	id data = [mainsection objectForKey:key];
//	
//
//	
//	JovesWebSectionViewController *detailView = [[JovesWebSectionViewController alloc] initWithData:data withHeader:subSectionHeader andSection:sectionType];
//	[self.navigationController pushViewController:detailView animated:YES];
//	
//
//	NSArray *temp = [data allKeysForObject:subsectionData];
//	NSString *somekey = [temp objectAtIndex:0];
//	
	
	
	
	
	/*NSDictionary *mainsection = [[JovesAppDelegate sharedAppDelegate].data.cachedData objectForKey:key];
	
	NSLog(@"%@",[NSString stringWithFormat:@"section%i",indexPath.row]);
	
	NSDictionary *subsection = [mainsection objectForKey:[NSString stringWithFormat:@"section%i",indexPath.row]]; 
	id findType = [subsection objectForKey:sectionName];
		
	NSString *subSectionHeader = [[_header stringByAppendingString:@" > "] stringByAppendingString:[[subsectionData objectAtIndex:indexPath.row] objectForKey:@"title"]];
	
	
	if ([findType isKindOfClass:[NSArray class]])
	{
	JovesSectionTableViewController *detailViewController = [[JovesSectionTableViewController alloc] initWithSection:indexPath.row andKey:key withHeader:subSectionHeader];
     [self.navigationController pushViewController:detailViewController animated:YES];
    }
	else if ([findType isKindOfClass:[NSString class]])
	{
	JovesWebSectionViewController *detailViewController = [[JovesWebSectionViewController alloc] initWithSection:indexPath.row andKey:key withHeader:subSectionHeader];
		[self.navigationController pushViewController:detailViewController animated:YES];
	}
	*/
}
-(NSString *)returntype:(NSString *)givetype {
	
	NSString *subsectionstring = [givetype.lowercaseString stringByAppendingString:@"subsections"];
	return subsectionstring;
}

@end
