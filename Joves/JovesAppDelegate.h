//
//  JovesAppDelegate.h
//  Joves
//
//  Created by Alok Sahay on 11/19/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Joves_data.h"
#import "Argol.h"
#import "Joves_POI.h"

@interface JovesAppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UITabBarController *tabBarController;
@property (strong, nonatomic) Joves_data *data;
@property (strong, nonatomic) ArgolDevice *gps;
@property (strong, nonatomic) NSMutableArray *targetPOI;

+ (JovesAppDelegate*) sharedAppDelegate;

-(void)startGPS;
-(void)loadTargets;
-(void)stopGPS;
-(void)reloadData;

@end
