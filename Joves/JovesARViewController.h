//
//  JovesARViewController.h
//  Joves
//
//  Created by Jan Baros on 12/20/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Joves_target.h"

#define METERS_PER_MILE 1609.344

@class ArgolTarget;

@interface JovesARViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,MKMapViewDelegate> {
	NSArray *arbuttons;
	UITableView *poiList;
	UITableView *flipList;
	NSArray *flipData;
	NSArray *_data;
	UIView *radarView;
	UIView *poiView;
	MKMapView *poiMap;
	UIButton *tematicButton;
	UIButton *districtsButton;
	//BOOL POIisPoint;
	NSArray *_tableData;
	BOOL categoryNotArea;
	NSRange rowRange;
	int selectedCase;
	NSArray *filteredItems;
	NSString *currentRow;
	Joves_target *selectedPOI; // currentPOI
	NSMutableArray *POIsOnMap;
}

@end
