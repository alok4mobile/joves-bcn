//
//  JovesWhiteCell.m
//  Joves
//
//  Created by Jan Baros on 11/21/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import "JovesWhiteCell.h"

@implementation JovesWhiteCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    
		self.textLabel.textColor = [Joves_Tools appBlueColor];
		self.textLabel.font = [Joves_Tools boldappFontofSize:16];
		self.textLabel.backgroundColor = [UIColor clearColor];
		self.textLabel.numberOfLines = 1;
			
		self.detailTextLabel.textColor = [Joves_Tools appGrayColor];
		self.detailTextLabel.font = [Joves_Tools appFontofSize:10];
		self.detailTextLabel.backgroundColor = [UIColor clearColor];
		self.detailTextLabel.numberOfLines = 0;
	
	}
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

	if (selected) {
		self.selectionStyle = UITableViewCellSelectionStyleGray;
	}
}

-(void)layoutSubviews {
	[super layoutSubviews];
	
	[self.textLabel setFrame:CGRectMake(self.textLabel.frame.origin.x + 10, 10, self.contentView.frame.size.width - self.textLabel.frame.origin.x - 10, self.textLabel.frame.size.height)];
	[self.detailTextLabel setFrame:CGRectMake(self.detailTextLabel.frame.origin.x + 10, 30, self.detailTextLabel.frame.size.width - 10, self.detailTextLabel.frame.size.height)];
	self.imageView.frame = CGRectMake(15, 15, CGRectGetHeight(self.frame)-50, CGRectGetHeight(self.frame)-50);
}


@end
