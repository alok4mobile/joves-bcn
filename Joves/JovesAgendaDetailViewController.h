//
//  JovesAgendaDetailViewController.h
//  Joves
//
//  Created by Jan Baros on 1/14/13.
//  Copyright (c) 2013 groupoadi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JovesAgendaDetailViewController : UIViewController <UIWebViewDelegate> {
	UIWebView *viewWeb;
	NSString *_header;
	NSString *_url;
}

- (id)initWithURL:(NSString*)URL withHeader:(NSString*)header;
@end
