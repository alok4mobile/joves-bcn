//
//  Joves_target.h
//  Joves
//
//  Created by Jan Baros on 12/20/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Joves_POI.h"
#import "ArgolTarget.h"

@interface Joves_target : NSObject

@property (nonatomic, retain) Joves_POI *poi;
@property (nonatomic, retain) ArgolTarget *target;

@end
