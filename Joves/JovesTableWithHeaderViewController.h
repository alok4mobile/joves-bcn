//
//  JovesTableWithHeaderViewController.h
//  Joves
//
//  Created by Jan Baros on 11/30/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JovesTableWithHeaderViewController : UITableViewController {
	NSArray *data;
	NSString *headerData;
	NSString *header;
}
- (id)initWithType:(NSString*)type;
@end
