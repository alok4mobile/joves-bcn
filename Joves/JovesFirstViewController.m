//
//  JovesFirstViewController.m
//  Joves
//
//  Created by Alok Sahay on 11/19/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import "JovesFirstViewController.h"
#import "JovesTableWithHeaderViewController.h"
#import "JovesMainSectionViewController.h"
#import "JovesARViewController.h"

@interface JovesFirstViewController ()


@end

@implementation JovesFirstViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		
		self.title = NSLocalizedString(@"Inici", nil);
        self.tabBarItem.image = [UIImage imageNamed:@"menu_inactive"];
		
	}
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self.navigationController setNavigationBarHidden:YES];
	
		
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	
    UIView *background = [[UIView alloc] initWithFrame:self.view.frame];
    
    UIImageView *backgroundPicture = [[UIImageView alloc] initWithFrame:background.frame];
    backgroundPicture.image = [UIImage imageNamed:@"IMAGE-BACKGROUND"];
    
    UIImageView *dotsImage = [[UIImageView alloc] initWithFrame:background.frame];
    dotsImage.image = [UIImage imageNamed:@"DOTS-BACKGROUND"];
    dotsImage.alpha = 0.2;
    
    CGFloat bcnlogoyoffset = 20;
    
    UIImageView *adBCNlogo = [[UIImageView alloc] initWithFrame:CGRectMake(0, bcnlogoyoffset, 55, 60)];
    adBCNlogo.image = [UIImage imageNamed:@"escut"];
	
    UIImageView *adBCNheader = [[UIImageView alloc] initWithFrame:CGRectMake(0, bcnlogoyoffset, 170, 60)];
    adBCNheader.image = [UIImage imageNamed:@"Ajuntmanet-de-Barcelona"];
    
    UIImageView *adBCNtitle = [[UIImageView alloc] initWithFrame:CGRectMake(35, 90, 255, 25)];
    adBCNtitle.image = [UIImage imageNamed:@"app_title-"];
    
    
    [background addSubview:backgroundPicture];
    [background addSubview:dotsImage];
    [background addSubview:adBCNheader];
    [background addSubview:adBCNlogo];
    [background addSubview:adBCNtitle];
    
    CGFloat buttonstartx = 28;
    CGFloat buttonstarty = 125;
    CGFloat buttonwidth = 75;
    CGFloat buttonheight = 90;
    CGFloat row1spacingx = 95;
    CGFloat rowheightspace = 95;
    
    agendaButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonstartx, buttonstarty, buttonwidth, buttonheight)];
	[agendaButton addTarget:self action:@selector(loadAgendaRSS) forControlEvents:UIControlEventTouchUpInside];
		
    formacioButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonstartx + row1spacingx, buttonstarty, buttonwidth, buttonheight)];
	[formacioButton addTarget:self action:@selector(loadformacion) forControlEvents:UIControlEventTouchUpInside];
	
    treballButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonstartx + 2*row1spacingx , buttonstarty, buttonwidth, buttonheight)];
	[treballButton addTarget:self action:@selector(loadtreball) forControlEvents:UIControlEventTouchUpInside];
    
    culturaButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonstartx, buttonstarty + rowheightspace, buttonwidth, buttonheight)];
	[culturaButton addTarget:self action:@selector(loadCultura) forControlEvents:UIControlEventTouchUpInside];
	
    sportsButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonstartx + row1spacingx, buttonstarty + rowheightspace, buttonwidth, buttonheight)];
	[sportsButton addTarget:self action:@selector(loadSports) forControlEvents:UIControlEventTouchUpInside];
	
    coopButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonstartx + 2*row1spacingx, buttonstarty + rowheightspace, buttonwidth, buttonheight)];
	[coopButton addTarget:self action:@selector(loadCooperacion) forControlEvents:UIControlEventTouchUpInside];
    
    particiButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonstartx + 50, buttonstarty + 2*rowheightspace, buttonwidth, buttonheight)];
    [particiButton addTarget:self action:@selector(loadParticipacion) forControlEvents:UIControlEventTouchUpInside];
    
    wifiButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonstartx + 50+ row1spacingx, buttonstarty + 2*rowheightspace,buttonwidth,buttonheight)];
	[wifiButton addTarget:self action:@selector(loadAR) forControlEvents:UIControlEventTouchUpInside];
	
	
    [self.view addSubview:background];
    [self.view addSubview:agendaButton];
    [self.view addSubview:formacioButton];
    [self.view addSubview:treballButton];
    [self.view addSubview:culturaButton];
    [self.view addSubview:sportsButton];
    [self.view addSubview:coopButton];
    [self.view addSubview:particiButton];
    [self.view addSubview:wifiButton];
	
}

-(void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:YES];
	
	UIImage *agendaNormal;
	UIImage *agendaHighlighted;
	
	UIImage *formacioNormal;
	UIImage *formacioHighlighted;
	
	UIImage *treballNormal;
	UIImage *treballHighlighted;
	
	UIImage *culturaNormal;
	UIImage *culturaHighlighted;
	
	UIImage *sportsNormal;
	UIImage *sportsHighlighted;
	
	UIImage *participacioNormal;
	UIImage *participacioHighlighted;
	
	UIImage *coopNormal;
	UIImage *coopHighlighted;
	
	UIImage *wifiNormal;
	UIImage *wifiHighlighted;
	
	language = [[NSLocale preferredLanguages] objectAtIndex:0];
	
	
	if ([language isEqualToString:@"ca"]) // for catalan
	{
		agendaNormal = [UIImage imageNamed:@"Agenda-off_cat"];
		agendaHighlighted = [UIImage imageNamed:@"Agenda-on_cat"];
		
		formacioNormal = [UIImage imageNamed:@"formacio_off_cat"];
		formacioHighlighted = [UIImage imageNamed:@"formacio-on_cat"];
		
		treballNormal = [UIImage imageNamed:@"Treball-off_cat"];
		treballHighlighted =[UIImage imageNamed:@"Treball-on_cat"];
		
		culturaNormal = [UIImage imageNamed:@"Culture-off_cat"];
		culturaHighlighted = [UIImage imageNamed:@"Culture-on_cat"];
		
		sportsNormal =[UIImage imageNamed:@"sports-off_cat"];
		sportsHighlighted =[UIImage imageNamed:@"sports-on_cat"];
		
		coopNormal = [UIImage imageNamed:@"Cooperacio-off_cat"];
		coopHighlighted = [UIImage imageNamed:@"Cooperacio-on_cat"];
		
		participacioNormal = [UIImage imageNamed:@"Participació-off_cat"];
		participacioHighlighted = [UIImage imageNamed:@"Participació-on_cat"];
		
		wifiNormal = [UIImage imageNamed:@"RA_button_OFF_CAT"];
		wifiHighlighted = [UIImage imageNamed:@"RA_button_ON_CAT"];
		
	}
	else if (![language isEqualToString:@"ca"]) // for spanish or anything else
	{
		agendaNormal = [UIImage imageNamed:@"Agenda-off-_cast"];
		agendaHighlighted = [UIImage imageNamed:@"Agenda-on-_cast"];
		
		formacioNormal = [UIImage imageNamed:@"formacion-off_cast"];
		formacioHighlighted = [UIImage imageNamed:@"formacion-on_cast"];
		
		treballNormal = [UIImage imageNamed:@"Treball-off_cast"];
		treballHighlighted =[UIImage imageNamed:@"Treball-on_cast"];
		
		culturaNormal = [UIImage imageNamed:@"Culture-off_cast"];
		culturaHighlighted = [UIImage imageNamed:@"Culture-on_cast"];
		
		sportsNormal =[UIImage imageNamed:@"sports-off_cast"];
		sportsHighlighted =[UIImage imageNamed:@"sports-on_cast"];
		
		coopNormal = [UIImage imageNamed:@"Cooperacio-off_cast"];
		coopHighlighted = [UIImage imageNamed:@"Cooperacio-on_cast"];
		
		participacioNormal = [UIImage imageNamed:@"Participació-off_cast"];
		participacioHighlighted = [UIImage imageNamed:@"Participació-on_cast"];
		
		wifiNormal = [UIImage imageNamed:@"RA_button_OFF_ESP"];
		wifiHighlighted = [UIImage imageNamed:@"RA_button_ON_ESP"];
	}
	
	[agendaButton setImage:agendaNormal forState:UIControlStateNormal];
    [agendaButton setImage:agendaHighlighted forState:UIControlStateHighlighted];
	
	[formacioButton setImage:formacioNormal forState:UIControlStateNormal];
    [formacioButton setImage:formacioHighlighted forState:UIControlStateHighlighted];
	
	[treballButton setImage:treballNormal forState:UIControlStateNormal];
    [treballButton setImage:treballHighlighted forState:UIControlStateHighlighted];
	
	[culturaButton setImage:culturaNormal forState:UIControlStateNormal];
    [culturaButton setImage:culturaHighlighted forState:UIControlStateHighlighted];
	
	[sportsButton setImage:sportsNormal forState:UIControlStateNormal];
    [sportsButton setImage:sportsHighlighted forState:UIControlStateHighlighted];
	
	[coopButton setImage:coopNormal forState:UIControlStateNormal];
    [coopButton setImage:coopHighlighted forState:UIControlStateHighlighted];
	
	[particiButton setImage:participacioNormal forState:UIControlStateNormal];
    [particiButton setImage:participacioHighlighted forState:UIControlStateHighlighted];
	
	[wifiButton setImage:wifiNormal forState:UIControlStateNormal];
    [wifiButton setImage:wifiHighlighted forState:UIControlStateHighlighted];

}

-(void)loadAgendaRSS {
	
	JovesMainSectionViewController *agendaRSSView = [[JovesMainSectionViewController alloc] initWithType:@"Agenda"];
	[self.navigationController pushViewController:agendaRSSView animated:YES];
}

-(void)loadAR {
	JovesARViewController *arView = [[JovesARViewController alloc] initWithNibName:nil bundle:nil];
	[self.navigationController pushViewController:arView animated:YES];
}

-(void)loadtreball {
	
	//NSString *section;
	
	if ([language isEqualToString:@"ca"]) // for catalan
	{
	}
	else if (![language isEqualToString:@"ca"]) // for spanish or anything else
	{
	}
	
	JovesMainSectionViewController *treballView = [[JovesMainSectionViewController alloc] initWithType:@"Treball"];
	
	[self.navigationController pushViewController:treballView animated:YES];
}

-(void)loadformacion {
	
	JovesMainSectionViewController *formacionView = [[JovesMainSectionViewController alloc] initWithType:@"Formació"];
	[self.navigationController pushViewController:formacionView animated:YES];
}

-(void)loadSports {
	
	JovesTableWithHeaderViewController *sportsView = [[JovesTableWithHeaderViewController alloc] initWithType:@"Esports"];
	
	[self.navigationController pushViewController:sportsView animated:YES];
}

-(void)loadCultura {
	
	JovesTableWithHeaderViewController *culturaView = [[JovesTableWithHeaderViewController alloc] initWithType:@"Cultura y Ocio"];
	
	[self.navigationController pushViewController:culturaView animated:YES];
}

-(void)loadCooperacion {
	
	JovesMainSectionViewController *coop = [[JovesMainSectionViewController alloc] initWithType:@"Cooperacion"];
	
	[self.navigationController pushViewController:coop animated:YES];
}

-(void)loadParticipacion {
	
	JovesTableWithHeaderViewController *participacion = [[JovesTableWithHeaderViewController alloc] initWithType:@"Participacion"];
	
	[self.navigationController pushViewController:participacion animated:YES];
}



@end
