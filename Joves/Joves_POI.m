//
//  Joves_POI.m
//  Joves
//
//  Created by Jan Baros on 12/20/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import "Joves_POI.h"

@implementation Joves_POI

@synthesize name = _name, latitude = _latitude, longitude = _longitude, area = _area, category = _category;

- (id) initWithDictionary:(NSDictionary*)value{
	self = [super init];
	if (self) {
		
		self.name = [value objectForKey:@"Name"];
		self.latitude = [value objectForKey:@"Lat"];
		self.longitude = [value objectForKey:@"Lon"];
		self.category  = [value objectForKey:@"Category"];
		self.area  = [value objectForKey:@"Neighborhood"];
	}
	return self;
	
}

@end
