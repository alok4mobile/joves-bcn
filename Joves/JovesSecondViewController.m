//
//  JovesSecondViewController.m
//  Joves
//
//  Created by Alok Sahay on 11/19/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import "JovesSecondViewController.h"

@interface JovesSecondViewController ()

@end

@implementation JovesSecondViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		self.title = NSLocalizedString(@"Mapa App", nil);
        self.tabBarItem.image = [UIImage imageNamed:@"mapa-app"];
    }
    return self;
}
							
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
	
	
	[super viewDidUnload];
}
@end
