//
//  NumberSmoother.h
//  LocationTest
//
//  Created by Aleš Podolník on 19/06/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NumberSmoother : NSObject
/*
 
 Trida pro vyhlazovani hodnoty pomoci aritmetickeho prumeru poslednich N zmerenych.
 
 Inicializuje se pomoci nasady a poctu N. Nasada se rozkopiruje do pocatecnich N hodnot.
 
 Pocet N se da v prubehu vyhlazovani zmenit.
 
 Pouziti:
 
 NumberSmoother s = [NumberSmoother initWithFloat:0.0 smoothingFactor:10];
 
 while (true) {
    float f = ZmerData();
    float vyhlazene = [s smoothFloat:f];
 } 
 */

@property (nonatomic) NSInteger smoothingFactor;
@property (nonatomic, strong) NSMutableArray *values;

- (id) initWithNumber:(NSNumber*)number smoothingFactor:(NSInteger)factor;
- (id) initWithFloat:(CGFloat)number smoothingFactor:(NSInteger)factor;
- (void) dealloc;

- (void) updateSmoothingFactor:(NSInteger)factor;

- (NSNumber*) smoothNative:(NSNumber*)number;
- (CGFloat) smooth:(CGFloat)number;

- (NSNumber*) lastNative;
- (CGFloat) last;

@end
