//
//  ARView.m
//  LocationTest
//
//  Created by Aleš Podolník on 22/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ArgolView.h"
#import "ArgolTarget.h"

@implementation ArgolView

@synthesize cameraFrame;

- (id) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
        
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        AVCaptureDeviceInput *newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:[self cameraWithPosition:AVCaptureDevicePositionBack] error:nil];
        
        AVCaptureSession *session = [[AVCaptureSession alloc] init];
        session.sessionPreset = AVCaptureSessionPreset640x480;
        if ([session canAddInput:newVideoInput]) {
            [session addInput:newVideoInput];
        }
        
        [session startRunning];
        AVCaptureVideoPreviewLayer *previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:session];
        
        previewLayer.frame = self.bounds;
        [self.layer addSublayer:previewLayer];
        self.cameraFrame = previewLayer.frame;
         NSLog(@"frame size <w:%f><h:%f>",previewLayer.frame.size.width,previewLayer.frame.size.height);
        
        
        
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Camera not available" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
        self.backgroundColor = [UIColor grayColor];
        
    }
    
    return self;
}

- (AVCaptureDevice *) cameraWithPosition:(AVCaptureDevicePosition) position {
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices) {
        if ([device position] == position) {
            return device;
        }
    }
    return nil;
}

- (CGPoint) getCenterFromCoordinates:(CGPoint)coords {
    if (self.cameraFrame.size.height == 0)  {
        
        double height = self.frame.size.height/2;
        double width = self.frame.size.width/2;
        
        double x = width + coords.x*width;
        double y = height + coords.y*height;
        return CGPointMake(x, y);
    }
    else {
        double coeff = self.cameraFrame.size.height/640;
        double height = (self.cameraFrame.size.height)/2;
        double width = (480 * coeff)/2;
        
        double x = self.cameraFrame.size.width/2 + coords.x*width;
        double y = height + coords.y*height;
        return CGPointMake(x, y);
    }
    
    return coords;
}

- (void) loadTargets {
	
}

@end
