//
//  ARLocationTarget.h
//  LocationTest
//
//  Created by Aleš Podolník on 22/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <QuartzCore/QuartzCore.h>
#import "MathUtils.h"
#import "NumberSmoother.h"

#define AR_TARGET_REACHED_DISTANCE 2 // default: 2 m

@class ArgolDevice;
@class ArgolView;

@interface ArgolTarget : NSObject

@property (nonatomic, strong) CLLocation* location;
@property (nonatomic, strong) ArgolDevice* device;

@property (nonatomic, strong) NSString* description;
@property (nonatomic) NSUInteger tag;

@property (nonatomic) double distance;
@property (nonatomic) double azimuth;

@property (nonatomic) Vector3D position;
@property (nonatomic) BOOL visible;

@property (nonatomic, strong) NumberSmoother *theta;
@property (nonatomic, strong) NumberSmoother *phi;

- (id) initWithDevice:(ArgolDevice *)device targetLocation:(CLLocation*)tarLocation;


// {* boo hoo *}//
- (void) check;
- (BOOL) checkDistance;
- (void) checkAzimuth;
- (BOOL) checkViewField;

- (double) getVerticalDistance;
- (CGPoint) getProjection;
- (CGSize) getSize;

- (void) updateSmoothingFactor:(NSInteger)newFactor;

- (NSComparisonResult) compareDistance:(ArgolTarget*)targetToCompare;

@end
