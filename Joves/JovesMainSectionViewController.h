//
//  JovesTreballViewController.h
//  Joves
//
//  Created by Jan Baros on 11/20/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface JovesMainSectionViewController : UITableViewController {
	NSArray *_data;
	NSString *sectionType;
	NSString *header;
}


- (id)initWithType:(NSString*)type;


@end
