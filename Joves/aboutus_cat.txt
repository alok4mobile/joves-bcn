La investigació i els textos d’aquesta aplicació han estat realitzats per:

Associació Conèixer Història

Direcció i coordinació de projecte:
Jordi Guixé i Corominas

Documentalistes:
Toni Contijoch i Domingo
Oriol López i Badell
Àngel Reguera i Guerrero

L’Associació Conèixer Història és una entitat sense ànim de lucre creada l’any 2008 amb
l’objectiu d’investigar i de difondre el passat per poder entendre millor el present i teixir un
futur més just i democràtic.

Els membres fundadors de l’associació són llicenciats en Història i diplomats en Cultura
Històrica i Comunicació. L’ACH és oberta a tothom qui vulgui ser-ne membre, col·laborar-hi
esporàdicament o fer suggeriments.

L’ACH, amb la seva activitat, vol dignificar i homenatjar aquelles persones que van lluitar
en contra del totalitarisme i a favor de la democràcia. Un altre objectiu de l’associació és
investigar i recuperar el patrimoni que constitueixen els anomenats «espais de memòria» on
s’hi van esdevenir fets transcendents per a la defensa de les llibertats individuals i col·lectives,
tant a Catalunya ―durant la II República, la Guerra Civil, el Franquisme i la Transició― com a la
resta d’Europa i arreu del món.

L’ACH du a terme projectes d’investigació, organitza activitats de difusió (xerrades, rutes,
passis de pel·lícules, etc.), realitza tasques de documentació per a produccions audiovisuals i
per a publicacions i col·labora amb la Universitat de Barcelona oferint pràctiques a estudiants
de màster. Aquesta pàgina web és l’espai de comunicació de l’entitat.