//
//  Joves_data.m
//  Joves
//
//  Created by Jan Baros on 11/21/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import "Joves_data.h"
#import "Joves_POI.h"
#import "KMXMLParser.h"

@implementation Joves_data

-(id)init {
	self = [super init];
	
	if (self) {
		_jovesData = [NSMutableDictionary new];
		_wifiPoints = [NSMutableArray new];
		
		NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
		
		if (![language isEqualToString:@"ca"]) {
			langString = @"es_ES";
		}
		else langString = [NSString stringWithFormat:@"%@_ES",language];
		
		//[self parsePages];
        
		NSString *path = [[NSBundle mainBundle] pathForResource:@"JOVES_AR" ofType:@"plist"];
		NSArray* plistData = [NSArray arrayWithContentsOfFile:path];
		
		for (NSDictionary *value in plistData) // load all name of events from the plist with white spaces and line breaks.
		{
			Joves_POI *poi = [[Joves_POI alloc] initWithDictionary:value];
			[_wifiPoints addObject:poi];
		}
		
		// load favorites
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		
		
		if ([language isEqualToString:@"ca"]) {
			favoritesPath = [documentsDirectory stringByAppendingPathComponent:@"favs_ca.plist"];
		}
		else favoritesPath = [documentsDirectory stringByAppendingPathComponent:@"favs_es.plist"];
		
		NSFileManager *fileManager = [NSFileManager defaultManager];
		
		if ([fileManager fileExistsAtPath: favoritesPath])
		{
			_favoritePages = [[NSMutableArray alloc] initWithContentsOfFile: favoritesPath];
		}
		else
		{
			_favoritePages = [[NSMutableArray alloc] init];
		}
		
	}
	return self;
}

-(NSArray *)wifiPoints {
	return _wifiPoints;
}

-(NSDictionary *)jovesData {
	return _jovesData;
}

-(NSDictionary *)cachedData {
	
	NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
	
	NSString *path;
	
	if ([language isEqualToString:@"ca"]) {
		path = [[NSBundle mainBundle] pathForResource:@"data_web_ca" ofType:@"plist"];
	}
	
	else path = [[NSBundle mainBundle] pathForResource:@"data_web_es" ofType:@"plist"];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	if ([fileManager fileExistsAtPath:path]) {
		return [NSDictionary dictionaryWithContentsOfFile:path];
	}
	return nil;
}

-(NSArray *)agendaRSS {
	NSString *agendaURL = @"http://w110.bcn.cat/portal/site/Joves/menuitem.fcc7c99428df85297ca47ca4a2ef8a0c/template.secPageTABPageDisplayRSS?vgnextoid=1f78829afa3d8210VgnVCM10000074fea8c0RCRD&channelName=Agenda&lang=ca_ES&ttl=86400000";
	
	KMXMLParser *parser = [[KMXMLParser alloc] initWithURL:agendaURL delegate:nil];
	NSMutableArray *results = parser.posts;
	return results;
}

-(void)parsePages {
	[self parseTreballHomePage];
	
	[self parseFormacioHomePage];
	
	[self parseSports];
	
	[self parseCultura];
	
	[self parseCooperacion];
	
	[self parseParticipacion];
	
	[self parseMoreSections];
	
	[self save];
}

-(void)parseTreballHomePage {
    
	
	NSString *treballURL = [NSString stringWithFormat:@"/portal/site/Joves/menuitem.fcc7c99428df85297ca47ca4a2ef8a0c/?vgnextoid=9d724c10a86a8210VgnVCM10000074fea8c0RCRD&vgnextchannel=9d724c10a86a8210VgnVCM10000074fea8c0RCRD&lang=%@",langString];
	NSArray *treballArray = [self tableScript:treballURL];
	NSLog(@"Link = %@",treballURL);
	[_jovesData setObject:treballArray forKey:@"Treball"];
	[self parsetreBallSubSections:treballArray];
	
}

-(void)parsetreBallSubSections:(NSArray*)treballArray
{
	NSMutableDictionary *treballSections = [NSMutableDictionary new];
	NSMutableDictionary *subsubsection = [NSMutableDictionary new];
	
	for (int i = 0; i < treballArray.count; ++i) {
		
		NSDictionary *subsection = [treballArray objectAtIndex:i];
		NSString *sectionNumber = [NSString stringWithFormat:@"section%i",i];
		
		if (i == 4 || i == 7)
		{
			NSArray *saveSubSectionData = [NSArray new];
			saveSubSectionData = [self tableScript:[subsection objectForKey:@"link"]];
			[treballSections setObject:saveSubSectionData forKey:sectionNumber];
			
			for (NSDictionary *SubDict in saveSubSectionData) {
				
				NSString *subKey = [SubDict objectForKey:@"title"];
				NSString *subLink = [SubDict objectForKey:@"link"];
				NSString *subsubData = [self textScript:subLink];
				
				[subsubsection setObject:subsubData forKey:subKey];
				
			}
			
		}
		else
		{
			NSString *saveSubSectionData;
			saveSubSectionData = [self textScript:[subsection objectForKey:@"link"]];
			[treballSections setObject:saveSubSectionData forKey:sectionNumber];
		}
		
	}
	NSLog(@"Treball Subsections");
	[_jovesData setObject:treballSections forKey:@"treballsubsections"];
	[_jovesData setObject:subsubsection forKey:@"treballsubsectionssubsections"];
}

-(void)parseFormacioHomePage {
	
	NSString *formaciolink = [NSString stringWithFormat:@"/portal/site/Joves/menuitem.e6bebd9281a7ff596f256f25a2ef8a0c/?vgnextoid=88df4c10a86a8210VgnVCM10000074fea8c0RCRD&vgnextchannel=88df4c10a86a8210VgnVCM10000074fea8c0RCRD&lang=%@",langString];
	NSArray *formacioHomePageData = [self tableScript:formaciolink];
	[_jovesData setObject:formacioHomePageData forKey:@"Formació"];
	NSLog(@"Formacio");
	[self parseFormacioSubSections:formacioHomePageData];
	
}

-(void)parseFormacioSubSections:(NSArray *)data {
	
	NSMutableDictionary *formacioSections = [NSMutableDictionary new];
	NSMutableDictionary *subsubsection = [NSMutableDictionary new];
	for (int i = 0; i < data.count; ++i) {
		
		NSDictionary *subsection = [data objectAtIndex:i];
		
		NSString *sectionNumber = [NSString stringWithFormat:@"section%i",i];
		
		if (i == 0 || i == 2)
			
			
		{
			NSArray *saveSubSectionData = [NSArray new];
			saveSubSectionData = [self tableScript:[subsection objectForKey:@"link"]];
			[formacioSections setObject:saveSubSectionData forKey:sectionNumber];
			
			for (NSDictionary *SubDict in saveSubSectionData) {
				
				NSString *subKey = [SubDict objectForKey:@"title"];
				NSString *subLink = [SubDict objectForKey:@"link"];
				NSString *subsubData = [self textScript:subLink];
				
				[subsubsection setObject:subsubData forKey:subKey];
				
			}
			
		}
		
		else
		{
			NSString *string =	[self textScript:[subsection objectForKey:@"link"]];
			[formacioSections setObject:string forKey:sectionNumber];
			
			
		}
		
	}
	
	NSLog(@"Formacio subsections");
	
	[_jovesData setObject:formacioSections forKey:@"formaciósubsections"];
	
	[_jovesData setObject:subsubsection forKey:@"formaciósubsectionssubsections"];
}

-(void)parseSports {
	
	NSString *sportsURL = [NSString stringWithFormat:@"/portal/site/Joves/menuitem.fcc7c99428df85297ca47ca4a2ef8a0c/?vgnextoid=429fa29f5a6a8210VgnVCM10000074fea8c0RCRD&vgnextchannel=429fa29f5a6a8210VgnVCM10000074fea8c0RCRD&lang=%@",langString];
    
	NSArray *sportsHomePageData = [self tableScript:sportsURL];
	NSString *sportsHeader  = [self textScript:sportsURL];
	
	NSMutableDictionary *sportsHome = [NSMutableDictionary new];
	
	[sportsHome setObject:sportsHomePageData forKey:@"content"];
	[sportsHome setObject:sportsHeader forKey:@"header"];
	
	[_jovesData setObject:sportsHome forKey:@"Esports"];
	NSLog(@"Sports");
	[self parseSportsSubSections:sportsHomePageData];
	
}

-(void)parseSportsSubSections:(NSArray *)data {
	
	NSMutableDictionary *sportsSections = [NSMutableDictionary new];
	for (int i = 0; i < data.count; ++i) {
		
		NSDictionary *subsection = [data objectAtIndex:i];
		NSString *sectionNumber = [NSString stringWithFormat:@"section%i",i];
		NSString *string =	[self textScript:[subsection objectForKey:@"link"]];
		[sportsSections setObject:string forKey:sectionNumber];
	}
	NSLog(@"Sports subsection");
	[_jovesData setObject:sportsSections forKey:@"esportssubsections"];
	
}

-(void)parseCultura {
	
	NSString *culturaURL = [NSString stringWithFormat:@"/portal/site/Joves/menuitem.fcc7c99428df85297ca47ca4a2ef8a0c/?vgnextoid=a3aba29f5a6a8210VgnVCM10000074fea8c0RCRD&vgnextchannel=a3aba29f5a6a8210VgnVCM10000074fea8c0RCRD&lang=%@",langString];
	
	NSArray *culturaHomePageData = [self tableScript:culturaURL];
	NSString *culturaHeaderdata = [self textScript:culturaURL];
	
	NSMutableDictionary *culturaHomeSections = [NSMutableDictionary new];
	[culturaHomeSections setObject:culturaHomePageData forKey:@"content"];
	[culturaHomeSections setObject:culturaHeaderdata forKey:@"header"];
	
	[_jovesData setObject:culturaHomeSections forKey:@"Cultura y Ocio"];
	NSLog(@"Cultura");
	[self parseCulturaSubSections:culturaHomePageData];
	
}

-(void)parseCulturaSubSections:(NSArray *)data {
	
	NSMutableDictionary *culturaSections = [NSMutableDictionary new];
	for (int i = 0; i < data.count; ++i) {
		
		NSDictionary *subsection = [data objectAtIndex:i];
		NSString *sectionNumber = [NSString stringWithFormat:@"section%i",i];
		NSString *string =	[self textScript:[subsection objectForKey:@"link"]];
		[culturaSections setObject:string forKey:sectionNumber];
	}
	NSLog(@"Cultura Subsection");
	[_jovesData setObject:culturaSections forKey:@"cultura y ociosubsections"];
	
}


-(void)parseCooperacion {
	
	NSString *cooperacionURL = [NSString stringWithFormat:@"/portal/site/Joves/menuitem.fcc7c99428df85297ca47ca4a2ef8a0c/?vgnextoid=11c6a29f5a6a8210VgnVCM10000074fea8c0RCRD&vgnextchannel=11c6a29f5a6a8210VgnVCM10000074fea8c0RCRD&lang=%@",langString];
	
	NSArray *cooperacionHomePageData = [self tableScriptMod:cooperacionURL];
	
	[_jovesData setObject:cooperacionHomePageData forKey:@"Cooperacion"];
	NSLog(@"Cooperacion");
	[self parseCooperacionSubSections:cooperacionHomePageData];
	
}

-(void)parseCooperacionSubSections:(NSArray *)data {
	
	NSMutableDictionary *cooperacionSections = [NSMutableDictionary new];
	for (int i = 0; i < data.count; ++i) {
		
		NSDictionary *subsection = [data objectAtIndex:i];
		
		NSString *sectionNumber = [NSString stringWithFormat:@"section%i",i];
		
		NSString *string =	[self textScript:[subsection objectForKey:@"link"]];
		[cooperacionSections setObject:string forKey:sectionNumber];
		
	}
	NSLog(@"Cooperacion Subsection");
	[_jovesData setObject:cooperacionSections forKey:@"cooperacionsubsections"];
	
}


-(void)parseParticipacion {
	
	NSString *participacionURL = [NSString stringWithFormat:@"/portal/site/Joves/menuitem.fcc7c99428df85297ca47ca4a2ef8a0c/?vgnextoid=1d2ea29f5a6a8210VgnVCM10000074fea8c0RCRD&vgnextchannel=1d2ea29f5a6a8210VgnVCM10000074fea8c0RCRD&lang=%@",langString];
	
	NSArray *participacionHomePageData = [self tableScript:participacionURL];
	NSString *participacionHeaderdata = [self textScript:participacionURL];
	
	NSMutableDictionary *participacionHomeSections = [NSMutableDictionary new];
	
	[participacionHomeSections setObject:participacionHomePageData forKey:@"content"];
	[participacionHomeSections setObject:participacionHeaderdata forKey:@"header"];
	
	[_jovesData setObject:participacionHomeSections forKey:@"Participacion"];
	NSLog(@"Participacion");
	[self parseParticipacionSubSections:participacionHomePageData];
	
}

-(void)parseParticipacionSubSections:(NSArray *)data {
	
	NSMutableDictionary *cooperacionSections = [NSMutableDictionary new];
	for (int i = 0; i < data.count; ++i) {
		
		NSDictionary *subsection = [data objectAtIndex:i];
		
		NSString *sectionNumber = [NSString stringWithFormat:@"section%i",i];
		
		NSString *string =	[self textScript:[subsection objectForKey:@"link"]];
		[cooperacionSections setObject:string forKey:sectionNumber];
		
	}
	NSLog(@"Participacion Subsection");
	[_jovesData setObject:cooperacionSections forKey:@"participacionsubsections"];
	
}


-(void)parseMoreSections {
	
	NSLog(@"More items: ");
	
	NSString *estades = [NSString stringWithFormat:@"/portal/site/Joves/menuitem.fcc7c99428df85297ca47ca4a2ef8a0c/?vgnextoid=d3c44c10a86a8210VgnVCM10000074fea8c0RCRD&vgnextchannel=d3c44c10a86a8210VgnVCM10000074fea8c0RCRD&lang=%@",langString];
	NSArray *estadesData = [self tableScript:estades];
	[_jovesData setObject:estadesData forKey:@"Estades a l'estranger"];
	[self parseEstadesSubSections:estadesData];
	
	NSString *habitatge = [NSString stringWithFormat:@"/portal/site/Joves/menuitem.fcc7c99428df85297ca47ca4a2ef8a0c/?vgnextoid=4fd54c10a86a8210VgnVCM10000074fea8c0RCRD&vgnextchannel=4fd54c10a86a8210VgnVCM10000074fea8c0RCRD&lang=%@",langString];
	NSArray *habitatgeData = [self tableScript:habitatge];
	[_jovesData setObject:habitatgeData forKey:@"Habitatge"];
	[self parseHabitatgeSubSections:habitatgeData];
	
	NSString *salut = [NSString stringWithFormat:@"/portal/site/Joves/menuitem.e6bebd9281a7ff596f256f25a2ef8a0c/?vgnextoid=dbe8a29f5a6a8210VgnVCM10000074fea8c0RCRD&vgnextchannel=dbe8a29f5a6a8210VgnVCM10000074fea8c0RCRD&lang=%@",langString];
	NSArray *salutData = [self tableScript:salut];
	[_jovesData setObject:salutData forKey:@"Salut"];
	[self parseSalutSubSections:salutData];
	
	NSString *turisme = [NSString stringWithFormat:@"/portal/site/Joves/menuitem.fcc7c99428df85297ca47ca4a2ef8a0c/?vgnextoid=f521a29f5a6a8210VgnVCM10000074fea8c0RCRD&vgnextchannel=f521a29f5a6a8210VgnVCM10000074fea8c0RCRD&lang=%@",langString];
	NSArray *turismeData = [self tableScript:turisme];
	[_jovesData setObject:turismeData forKey:@"Turisme"];
	[self parseTurismeSubSections:turismeData];
	
	NSString *transport = [NSString stringWithFormat:@"/portal/site/Joves/menuitem.fcc7c99428df85297ca47ca4a2ef8a0c/?vgnextoid=5ae3a29f5a6a8210VgnVCM10000074fea8c0RCRD&vgnextchannel=5ae3a29f5a6a8210VgnVCM10000074fea8c0RCRD&lang=%@",langString];
	NSArray *transportContent = [self tableScript:transport];
	NSString *transportHeader = [self textScript:transport];
	NSMutableDictionary *transportData = [NSMutableDictionary new];
	[transportData setObject:transportContent forKey:@"content"];
	[transportData setObject:transportHeader forKey:@"header"];
	[_jovesData setObject:transportData forKey:@"Mobilitat i Transport"];
	[self parseTransportSubSections:transportContent];
    
    
    
	NSString *ambient =  [NSString stringWithFormat:@"/portal/site/Joves/menuitem.fcc7c99428df85297ca47ca4a2ef8a0c/?vgnextoid=19095946fa6a8210VgnVCM10000074fea8c0RCRD&vgnextchannel=19095946fa6a8210VgnVCM10000074fea8c0RCRD&lang=%@",langString];
	NSArray *ambientContent = [self tableScript:ambient];
	NSString *ambientHeader = [self textScript:ambient];
	NSMutableDictionary *ambientData = [NSMutableDictionary new];
	[ambientData setObject:ambientContent forKey:@"content"];
	[ambientData setObject:ambientHeader forKey:@"header"];
	[_jovesData setObject:ambientData forKey:@"Medi Ambient i Sostenibilitat"];
	[self parseAmbientSubSections:ambientContent];
    
    
	NSString *publications = [NSString stringWithFormat:@"/portal/site/Joves/menuitem.fcc7c99428df85297ca47ca4a2ef8a0c/?vgnextoid=187be69964b06310VgnVCM10000072fea8c0RCRD&vgnextchannel=187be69964b06310VgnVCM10000072fea8c0RCRD&lang=%@",langString];
	NSArray *publicationsData = [self tableScript:publications];
	[_jovesData setObject:publicationsData forKey:@"Publicacions"];
	[self parsePublicationsSubSections:publicationsData];
}

-(void)parseEstadesSubSections:(NSArray *)data {
	NSLog(@"Estedas Subsection");
	NSMutableDictionary *estedasSections = [NSMutableDictionary new];
	NSMutableDictionary *subsubsection = [NSMutableDictionary new];
    
    for (int i = 0; i < data.count; ++i) {
		
		NSDictionary *subsection = [data objectAtIndex:i];
		
		NSString *sectionNumber = [NSString stringWithFormat:@"section%i",i];
		
		if (i ==0) {
			NSArray *saveSubSectionData = [self tableScript:[subsection objectForKey:@"link"]];
			[estedasSections setObject:saveSubSectionData forKey:sectionNumber];
            
            for (NSDictionary *SubDict in saveSubSectionData){
                
                NSString *subKey = [SubDict objectForKey:@"title"];
                NSString *subLink = [SubDict objectForKey:@"link"];
                NSString *subsubData = [self textScript:subLink];
                
                [subsubsection setObject:subsubData forKey:subKey];
            }
            
            
		}
		else {
			NSString *estedasWeb =	[self textScript:[subsection objectForKey:@"link"]];
			[estedasSections setObject:estedasWeb forKey:sectionNumber];
		}
	}
	[_jovesData setObject:estedasSections forKey:@"estades a l'estrangersubsections"];
    [_jovesData setObject:subsubsection forKey:@"estades a l'estrangersubsectionssubsections"];
}

-(void)parseHabitatgeSubSections:(NSArray *)data {
	NSLog(@"Habitat Subsection");
	NSMutableDictionary *habitatgeSections = [NSMutableDictionary new];
	NSMutableDictionary *subsubsection = [NSMutableDictionary new];
    
    for (int i = 0; i < data.count; ++i) {
		
		NSDictionary *subsection = [data objectAtIndex:i];
		
		NSString *sectionNumber = [NSString stringWithFormat:@"section%i",i];
		
		if (i ==0) {
			NSArray *table = [self tableScript:[subsection objectForKey:@"link"]];
			[habitatgeSections setObject:table forKey:sectionNumber];
            
            for (NSDictionary *SubDict in table){
                
                NSString *subKey = [SubDict objectForKey:@"title"];
                NSString *subLink = [SubDict objectForKey:@"link"];
                NSString *subsubData = [self textScript:subLink];
                
                [subsubsection setObject:subsubData forKey:subKey];
            }

            
            
		}
		else {
			NSString *web =	[self textScript:[subsection objectForKey:@"link"]];
			[habitatgeSections setObject:web forKey:sectionNumber];
		}
	}
	[_jovesData setObject:habitatgeSections forKey:@"habitatgesubsections"];
    [_jovesData setObject:subsubsection forKey:@"habitatgesubsectionssubsections"];
}

-(void)parseSalutSubSections:(NSArray *)data {
	NSLog(@"Salut Subsection");
	NSMutableDictionary *salutSections = [NSMutableDictionary new];
	NSMutableDictionary *subsubsection = [NSMutableDictionary new];
    
    for (int i = 0; i < data.count; ++i) {
		
		NSDictionary *subsection = [data objectAtIndex:i];
		NSString *sectionNumber = [NSString stringWithFormat:@"section%i",i];
		if (i ==0) {
			NSArray *table = [self tableScript:[subsection objectForKey:@"link"]];
			[salutSections setObject:table forKey:sectionNumber];
            
            for (NSDictionary *SubDict in table){
                
                NSString *subKey = [SubDict objectForKey:@"title"];
                NSString *subLink = [SubDict objectForKey:@"link"];
                NSString *subsubData = [self textScript:subLink];
                
                [subsubsection setObject:subsubData forKey:subKey];
            }

            
		}
		else {
			NSString *web =	[self textScript:[subsection objectForKey:@"link"]];
			[salutSections setObject:web forKey:sectionNumber];
		}
	}
	[_jovesData setObject:salutSections forKey:@"salutsubsections"];
    [_jovesData setObject:subsubsection forKey:@"salutsubsectionssubsections"];
}

-(void)parseTransportSubSections:(NSArray *)data {
	NSLog(@"Transport Subsection");
	NSMutableDictionary *transportSections = [NSMutableDictionary new];
	for (int i = 0; i < data.count; ++i) {
		NSDictionary *subsection = [data objectAtIndex:i];
		NSString *sectionNumber = [NSString stringWithFormat:@"section%i",i];
		NSString *web =	[self textScript:[subsection objectForKey:@"link"]];
		[transportSections setObject:web forKey:sectionNumber];
	}
	[_jovesData setObject:transportSections forKey:@"mobilitat i Transportsubsections"];
}

-(void)parseAmbientSubSections:(NSArray *)data {
	NSLog(@"Ambient Subsection");
	NSMutableDictionary *subSections = [NSMutableDictionary new];
	for (int i = 0; i < data.count; ++i) {
		NSDictionary *subsection = [data objectAtIndex:i];
		NSString *sectionNumber = [NSString stringWithFormat:@"section%i",i];
		NSString *web =	[self textScript:[subsection objectForKey:@"link"]];
		[subSections setObject:web forKey:sectionNumber];
	}
	[_jovesData setObject:subSections forKey:@"medi Ambient i Sostenibilitatsubsections"];
}


-(void)parseTurismeSubSections:(NSArray *)data {
	NSLog(@"Turisme Subsection");
	NSMutableDictionary *turismeSections = [NSMutableDictionary new];
	for (int i = 0; i < data.count; ++i) {
		NSDictionary *subsection = [data objectAtIndex:i];
		NSString *sectionNumber = [NSString stringWithFormat:@"section%i",i];
		NSString *web =	[self textScript:[subsection objectForKey:@"link"]];
		[turismeSections setObject:web forKey:sectionNumber];
	}
	[_jovesData setObject:turismeSections forKey:@"turismesubsections"];
}

-(void)parsePublicationsSubSections:(NSArray *)data {
	NSLog(@"Publications Subsection");
	NSMutableDictionary *publicationsSections = [NSMutableDictionary new];
	for (int i = 0; i < data.count; ++i) {
		NSDictionary *subsection = [data objectAtIndex:i];
		NSString *sectionNumber = [NSString stringWithFormat:@"section%i",i];
		NSString *web =	[self textScript:[subsection objectForKey:@"link"]];
		[publicationsSections setObject:web forKey:sectionNumber];
	}
	[_jovesData setObject:publicationsSections forKey:@"publicacionssubsections"];
}



-(NSString *)textScript:(NSString*)forLink
{
	NSString *parseLink = [NSString stringWithFormat:@"http://w110.bcn.cat%@",forLink];
	NSURL *treballsubsectionURL = [NSURL URLWithString:parseLink];
	NSData *treballData = [NSData dataWithContentsOfURL:treballsubsectionURL];
	
	NSString *strData = [[NSString alloc]initWithData:treballData encoding:NSUTF8StringEncoding];
	return strData;
}

-(NSArray *)tableScript:(NSString*)forLink {
	
	NSString *parseLink = [NSString stringWithFormat:@"http://w110.bcn.cat%@",forLink];
	
	// parseLink is the link needed to parse the rest of the page
	
	NSURL *treballsubsectionURL = [NSURL URLWithString:parseLink];
	
	NSData *treballData = [NSData dataWithContentsOfURL:treballsubsectionURL];
	
	TFHpple *treballParser = [TFHpple hppleWithHTMLData:treballData];
	
	// Left Column
	
	NSString *treballXpathQueryStringLeft = @"//div[@class='col-0']/div";
	NSArray *treballSectionDataLeft = [treballParser searchWithXPathQuery:treballXpathQueryStringLeft];
	
	NSMutableArray *sectionTable = [NSMutableArray new];
	
	for (TFHppleElement *element in treballSectionDataLeft) {
        
        NSMutableDictionary *treball = [[NSMutableDictionary alloc] init];
        
		for ( TFHppleElement *child in element.children ) {
			
			if ([child.tagName isEqualToString:@"img"]) {
				//	image
				[treball setObject:[child objectForKey:@"src"] forKey:@"image"];
				
			}
			
			if ([child.tagName isEqualToString:@"p"]) {
				// description
				
				NSString *detail = [[child firstChild] content];
				[treball setObject:detail forKey:@"description"];
				
			}
			if ([child.tagName isEqualToString:@"h3"]) {
				
				for ( TFHppleElement *child2 in child.children ) {
					
					if ([child2 objectForKey:@"href"] && [[child2 firstChild] content]) {
						
						//Subheading Title and Link
						
						NSString *link = [child2 objectForKey:@"href"];
						NSString *subtitle = [[child2 firstChild] content];
						[treball setObject:link forKey:@"link"];
						[treball setObject:subtitle forKey:@"title"];
					}
				}
			}
		}
		[sectionTable addObject:treball];
	}
	
	NSString *treballXpathQueryStringRight = @"//div[@class='col-1']/div";
	NSArray *treballSectionDataRight = [treballParser searchWithXPathQuery:treballXpathQueryStringRight];
	
	for (TFHppleElement *element in treballSectionDataRight) {
        
        NSMutableDictionary *treball = [[NSMutableDictionary alloc] init];
        
		for ( TFHppleElement *child in element.children ) {
			
			if ([child.tagName isEqualToString:@"img"]) {
				//	image
				[treball setObject:[child objectForKey:@"src"] forKey:@"image"];
			}
			if ([child.tagName isEqualToString:@"p"]) {
				// description
				
				NSString *detail = [[child firstChild] content];
				[treball setObject:detail forKey:@"description"];
			}
			if ([child.tagName isEqualToString:@"h3"]) {
				
				for ( TFHppleElement *child2 in child.children ) {
					
					if ([child2 objectForKey:@"href"] && [[child2 firstChild] content]) {
						
						//Subheading Title and Link
						
						NSString *link = [child2 objectForKey:@"href"];
						NSString *subtitle = [[child2 firstChild] content];
						[treball setObject:link forKey:@"link"];
						[treball setObject:subtitle forKey:@"title"];
					}
				}
			}
		}
		[sectionTable addObject:treball];
	}
	
	return sectionTable;
}

-(NSArray *)tableScriptMod:(NSString*)forLink {
	
	NSString *parseLink = [NSString stringWithFormat:@"http://w110.bcn.cat%@",forLink];
	
	NSURL *treballsubsectionURL = [NSURL URLWithString:parseLink];
	
	NSData *treballData = [NSData dataWithContentsOfURL:treballsubsectionURL];
	
	TFHpple *treballParser = [TFHpple hppleWithHTMLData:treballData];
	
	// Left Column
	
	NSString *treballXpathQueryStringLeft = @"//div[@class='col-0']";
	NSArray *treballSectionDataLeft = [treballParser searchWithXPathQuery:treballXpathQueryStringLeft];
	
	NSMutableArray *sectionTable = [NSMutableArray new];
	
	for (TFHppleElement *element in treballSectionDataLeft) {
        
        NSMutableDictionary *table1 = [[NSMutableDictionary alloc] init];
        
		for ( TFHppleElement *child in element.children ) {
			
			
			if ([child.tagName isEqualToString:@"h3"]) {
				
				for ( TFHppleElement *child2 in child.children ) {
					
					if ([child2 objectForKey:@"href"] && [[child2 firstChild] content]) {
						
						//Subheading Title and Link
						
						NSString *link = [child2 objectForKey:@"href"];
						NSString *subtitle = [[child2 firstChild] content];
						[table1 setObject:link forKey:@"link"];
						[table1 setObject:subtitle forKey:@"title"];
					}
				}
			}
			
			if ([child.tagName isEqualToString:@"div"]) {
				
				for ( TFHppleElement *child2 in child.children ) {
					
					if ([child2.tagName isEqualToString:@"img"]) {
						
						[table1 setObject:[child2 objectForKey:@"src"] forKey:@"image"];
						
					}
					
					if ([child2.tagName isEqualToString:@"div"]) {
						
						for (TFHppleElement *child3 in child2.children) {
							if ([child3.tagName isEqualToString:@"p"]) {
								// description
								
								NSString *detail = [[child3 firstChild] content];
								[table1 setObject:detail forKey:@"description"];
							}
						}
					}
				}
				
				[sectionTable addObject:[table1 copy]];
				[table1 removeAllObjects];
			}
		}
	}
	
	NSString *treballXpathQueryStringRight = @"//div[@class='col-1']";
	NSArray *treballSectionDataRight = [treballParser searchWithXPathQuery:treballXpathQueryStringRight];
	
	for (TFHppleElement *element in treballSectionDataRight) {
		
		NSMutableDictionary *table2 = [[NSMutableDictionary alloc] init];
		
		for ( TFHppleElement *child in element.children ) {
			
			
			if ([child.tagName isEqualToString:@"h3"]) {
				
				for ( TFHppleElement *child2 in child.children ) {
					
					if ([child2 objectForKey:@"href"] && [[child2 firstChild] content]) {
						
						//Subheading Title and Link
						
						NSString *link = [child2 objectForKey:@"href"];
						NSString *subtitle = [[child2 firstChild] content];
						[table2 setObject:link forKey:@"link"];
						[table2 setObject:subtitle forKey:@"title"];
					}
				}
			}
			
			if ([child.tagName isEqualToString:@"div"]) {
				
				for ( TFHppleElement *child2 in child.children ) {
					
					if ([child2.tagName isEqualToString:@"img"]) {
						
						[table2 setObject:[child2 objectForKey:@"src"] forKey:@"image"];
						
					}
					
					if ([child2.tagName isEqualToString:@"div"]) {
						
						for (TFHppleElement *child3 in child2.children) {
							if ([child3.tagName isEqualToString:@"p"]) {
								// description
								
								NSString *detail = [[child3 firstChild] content];
								[table2 setObject:detail forKey:@"description"];
							}
						}
					}
				}
				
				[sectionTable addObject:[table2 copy]];
				[table2 removeAllObjects];
			}
		}
	}
	return sectionTable;
}

-(void)save {
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path;
	
	NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
	
	if ([language isEqualToString:@"ca"]) { // case of not catalan
		path = [documentsDirectory stringByAppendingPathComponent:@"data_web_ca.plist"];
	}
	else path = [documentsDirectory stringByAppendingPathComponent:@"data_web_es.plist"];
	
	
	
	[_jovesData writeToFile:path atomically:YES];
	NSLog(@"data saved at path %@!!",path);
}
-(void)addFavoritePage:(NSDictionary *)pageData {
	[_favoritePages addObject:pageData];
	[self saveFavorites];
}
-(void)removeFavoritePage:(NSDictionary *)pageData {
	[_favoritePages removeObject:pageData];
	[self saveFavorites];
}
-(BOOL)isPageFavorite:(NSDictionary *)pageData {
	return [_favoritePages containsObject:pageData];
}

-(void)saveFavorites {
	[_favoritePages writeToFile: favoritesPath atomically:YES];
}
-(NSArray *)favoritePages {
	
	/* keys:
	 [favData setObject:_data forKey:@"content"];
	 [favData setObject:_header forKey:@"name"];
	 [favData setObject:sectionType forKey:@"section"];*/
	NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"section" ascending:YES];
	
	NSArray *sortedArrray = [_favoritePages sortedArrayUsingDescriptors:@[descriptor]];
	
	return sortedArrray;
}


@end
