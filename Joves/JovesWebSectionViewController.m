//
//  JovesTreballLevel1ViewController.m
//  Joves
//
//  Created by Jan Baros on 11/21/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import "JovesWebSectionViewController.h"

@interface JovesWebSectionViewController ()

@end

@implementation JovesWebSectionViewController

- (id)initWithData:(NSString*)data withHeader:(NSString*)header andSection:(NSString*)section
{
    self = [super init];
    if (self) {
//		_data = [data stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		_data = data;
		_header = header;
		sectionType = section;
		favData = [NSMutableDictionary new];
		[favData setObject:_data forKey:@"content"];
		[favData setObject:_header forKey:@"name"];
		[favData setObject:sectionType forKey:@"section"];
		
	}
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:YES];
	self.tabBarController.navigationItem.title = _header;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// Do any additional setup after loading the view.
	
	self.view.backgroundColor = [UIColor whiteColor];
	
	CGFloat pagewidth = self.view.frame.size.width;
	CGFloat pageheight = self.view.frame.size.height;
	
	CGFloat headerOffset = 40;
	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, pagewidth, headerOffset)];
	headerView.backgroundColor = [Joves_Tools appBlueColor];
	
	UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, pagewidth-10, headerOffset)];
	headerLabel.text = _header;
	headerLabel.backgroundColor = [UIColor clearColor];
	headerLabel.font = [Joves_Tools appFontofSize:16];
	headerLabel.textColor = [UIColor whiteColor];
	[headerView addSubview:headerLabel];
	
	viewWeb= [[UIWebView alloc] initWithFrame:CGRectMake(0, headerOffset, pagewidth, (pageheight - 100))];
    NSLog(@"%@",_data);
	[viewWeb loadHTMLString:_data baseURL:nil];
	viewWeb.scrollView.backgroundColor = [UIColor whiteColor];
	[self.view addSubview:headerView];
	[self.view addSubview:viewWeb];
	viewWeb.delegate = self;
	viewWeb.hidden = YES;
		
		
	UIButton *favButton = [[UIButton alloc] initWithFrame:CGRectMake(270, 50, 30, 30)];
	[favButton setImage:[UIImage imageNamed:@"favorite-OFF"] forState:UIControlStateNormal];
	[favButton setImage:[UIImage imageNamed:@"favorite-ON"] forState:UIControlStateSelected];
	favButton.backgroundColor = [UIColor clearColor];
	[favButton addTarget:self action:@selector(favButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
	
	pageAlreadyFav = [[JovesAppDelegate sharedAppDelegate].data isPageFavorite:favData];
	
	if (pageAlreadyFav) {
		[favButton setSelected:YES];
	}
	
	[self.view addSubview:favButton];
	
}
-(void)favButtonPressed:(id)sender {
	
	pageAlreadyFav = [[JovesAppDelegate sharedAppDelegate].data isPageFavorite:favData];
	
	if (pageAlreadyFav) {
		[self removePage];
		[sender setSelected:NO];
	}
	else if	(!pageAlreadyFav){
		[self addPage];
		[sender setSelected:YES];
	}
}

-(void)addPage {
	[[JovesAppDelegate sharedAppDelegate].data addFavoritePage:favData];
}

-(void)removePage {
	[[JovesAppDelegate sharedAppDelegate].data removeFavoritePage:favData];
}
-(void)viewDidUnload {
	[super viewDidUnload];
	self.tabBarController.navigationItem.title = nil;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
	NSString *script;
    //NSString *script = @"document.getElementById('llistat-documents').innerHTML";
	
	if ([sectionType isEqualToString:@"Esports"]) {
		script= @"document.getElementById('revistas').innerHTML";
	}
	else script = @"document.getElementById('detall-noticia').innerHTML";
	
	
	script = [webView stringByEvaluatingJavaScriptFromString:script];
	
	if (![script isEqualToString:@""])
	{
		
		script = [[@"<html><head><style> * {font-family: Helvetica;} </style></head><body>" stringByAppendingString:script ] stringByAppendingString:@"</body></html>"];
		[viewWeb loadHTMLString:script baseURL:nil];
		NSLog(@"script <%@>",script);
		viewWeb.hidden = NO;
	}
	
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	if (navigationType == UIWebViewNavigationTypeLinkClicked) {
		[[UIApplication sharedApplication] openURL:[request URL]];
	}
	return YES;
}

@end
