//
//  JovesAboutGrupoADIViewController.m
//  Joves
//
//  Created by Jan Baros on 2/1/13.
//  Copyright (c) 2013 groupoadi. All rights reserved.
//

#import "JovesAboutGrupoADIViewController.h"

@interface JovesAboutGrupoADIViewController ()

@end

@implementation JovesAboutGrupoADIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	[self.navigationController setNavigationBarHidden:NO];
	[textScroll removeFromSuperview];
	
	self.view.backgroundColor = [UIColor whiteColor];
	
	NSString *contentOfFile;
	
	NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
	NSLog(@"localeIdentifier: <%@>", language);
	
	NSString *filePath;
	
	if ([language isEqualToString:@"ca"])
	{
		filePath = [[NSBundle mainBundle] pathForResource:@"aboutus_cat" ofType:@"txt"];
	}
	else
	{
		filePath = [[NSBundle mainBundle] pathForResource:@"aboutus_es" ofType:@"txt"];
	}
	if (filePath)
	{
		contentOfFile = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
	}
	
	CGSize maximumLabelSize = CGSizeMake(280,9000);
	CGSize expectedLabelSize = [contentOfFile sizeWithFont:[UIFont fontWithName:@"HelveticaNeue" size:13] constrainedToSize:maximumLabelSize lineBreakMode:UILineBreakModeWordWrap];
	CGRect aboutUsFrame;
	aboutUsFrame.size.height = expectedLabelSize.height;
	
	UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(20,20,280,aboutUsFrame.size.height)];
	textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
	textLabel.text = contentOfFile;
	textLabel.textAlignment = UITextAlignmentLeft;
	textLabel.numberOfLines = 0;
	
	textScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 45)];
	textScroll.contentSize = CGSizeMake(self.view.frame.size.width, 25 + aboutUsFrame.size.height);
	
	[textScroll addSubview:textLabel];
	[self.view addSubview:textScroll];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
