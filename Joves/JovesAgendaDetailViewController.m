//
//  JovesAgendaDetailViewController.m
//  Joves
//
//  Created by Jan Baros on 1/14/13.
//  Copyright (c) 2013 groupoadi. All rights reserved.
//

#import "JovesAgendaDetailViewController.h"

@interface JovesAgendaDetailViewController ()

@end

@implementation JovesAgendaDetailViewController

- (id)initWithURL:(NSString*)URL withHeader:(NSString*)header;
{
    self = [super init];
    if (self) {
        
		_url = [URL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];;
		_header = header;
		
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	CGFloat pagewidth = self.view.frame.size.width;
	CGFloat pageheight = self.view.frame.size.height;
	
	CGFloat headerOffset = 40;
	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, pagewidth, headerOffset)];
	headerView.backgroundColor = [Joves_Tools appBlueColor];
	
	UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, pagewidth-10, headerOffset)];
	headerLabel.text = _header;
	headerLabel.backgroundColor = [UIColor clearColor];
	headerLabel.font = [Joves_Tools appFontofSize:16];
	headerLabel.textColor = [UIColor whiteColor];
	[headerView addSubview:headerLabel];
	
	self.view.backgroundColor = [UIColor whiteColor];
	
	viewWeb= [[UIWebView alloc] initWithFrame:CGRectMake(0, headerOffset, pagewidth, (pageheight - 100))];
	viewWeb.backgroundColor = [UIColor whiteColor];
	[viewWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_url]]];
	viewWeb.scrollView.backgroundColor = [UIColor whiteColor];
	[self.view addSubview:headerView];
	[self.view addSubview:viewWeb];
	viewWeb.delegate = self;
	
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
	NSString *script = @"document.getElementById('modul-0').innerHTML";
    script = [webView stringByEvaluatingJavaScriptFromString:script];
	
	if (![script isEqualToString:@""])
	{
		
		script = [[@"<html><head><style> * {font-family: Helvetica;} </style></head><body>" stringByAppendingString:script ] stringByAppendingString:@"</body></html>"];
		[viewWeb loadHTMLString:script baseURL:nil];
		NSLog(@"script <%@>",script);
		viewWeb.hidden = NO;
	}
	
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	if (navigationType == UIWebViewNavigationTypeLinkClicked) {
		[[UIApplication sharedApplication] openURL:[request URL]];
	}
	return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
