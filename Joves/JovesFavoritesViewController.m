//
//  JovesFavoritesViewController.m
//  Joves
//
//  Created by Alok Sahay on 11/19/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import "JovesFavoritesViewController.h"
#import "JovesWebSectionViewController.h"

@interface JovesFavoritesViewController ()

@end

@implementation JovesFavoritesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Favs", nil);
        self.tabBarItem.image = [UIImage imageNamed:@"favorits_inactive"];
		data = [JovesAppDelegate sharedAppDelegate].data.favoritePages;
	}
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	_tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 56) style:UITableViewStylePlain];
	_tableView.backgroundColor = [UIColor whiteColor];
	_tableView.separatorColor = [UIColor grayColor];
	_tableView.delegate = self;
	_tableView.dataSource = self;
	_tableView.rowHeight = 40;
	
	[self.view addSubview:_tableView];
	[self reloadData];
}

-(void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	[self.navigationController setNavigationBarHidden:NO];
	[self reloadData];
	[_tableView reloadData];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return data.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
	NSDictionary *favData= [data objectAtIndex:indexPath.row];
	
	NSString *cellIdentifier = @"Cell Identifier";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	cell = nil;
	if (cell==nil) {
		cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
		cell.textLabel.text = [favData objectForKey:@"section"];
		cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
		cell.textLabel.textColor = [Joves_Tools appBlueColor];
		
		cell.detailTextLabel.text = [favData objectForKey:@"name"];
		cell.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
		cell.detailTextLabel.textColor = [UIColor grayColor];
		
		cell.backgroundColor = [UIColor clearColor];
		cell.contentView.backgroundColor = [UIColor clearColor];
		//cell.imageView.image = [UIImage imageNamed:@"ICON_FAVORITE_OFF.png"];
	}
	return cell;
}


-(void)reloadData
{
	data = nil;
	data = [JovesAppDelegate sharedAppDelegate].data.favoritePages;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSDictionary *favData= [data objectAtIndex:indexPath.row];
	JovesWebSectionViewController *detailView = [[JovesWebSectionViewController alloc] initWithData:[favData objectForKey:@"content"] withHeader:[favData objectForKey:@"name"] andSection:[favData objectForKey:@"section"]];
	if (detailView) {
		[self.navigationController pushViewController:detailView animated:YES];
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
