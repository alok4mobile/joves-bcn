//
//  Joves_POI.h
//  Joves
//
//  Created by Jan Baros on 12/20/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Joves_POI : NSObject

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *area;
@property (nonatomic, retain) NSString *category;
@property (nonatomic, retain) NSNumber *longitude;
@property (nonatomic, retain) NSNumber *latitude;

- (id) initWithDictionary:(NSDictionary*)dictionary;

@end
