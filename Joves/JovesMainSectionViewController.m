//
//  JovesTreballViewController.m
//  Joves
//
//  Created by Jan Baros on 11/20/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import "JovesMainSectionViewController.h"
#import "JovesWebSectionViewController.h"
#import "JovesSectionTableViewController.h"
#import "JovesAgendaDetailViewController.h"

@interface JovesMainSectionViewController ()

@end

@implementation JovesMainSectionViewController

- (id)initWithType:(NSString*)type
{
    self = [super init];
    if (self) {
        if ([type isEqualToString:@"Agenda"]) {
			_data = [JovesAppDelegate sharedAppDelegate].data.agendaRSS;
		}
		else _data = [[JovesAppDelegate sharedAppDelegate].data.cachedData objectForKey:type];
		
		sectionType = type;
	}
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	NSLog(@"data items: %i",_data.count);
	
}

-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self.navigationController setNavigationBarHidden:NO];
	self.title = sectionType;
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	
    // Return the number of rows in the section.
    return _data.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 100;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 40;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	
	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 70)];
	headerView.backgroundColor = [Joves_Tools appBlueColor];
	
	UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, headerView.bounds.size.width, 40)];
	headerLabel.backgroundColor = [UIColor clearColor];
	
	header = [@"Home > " stringByAppendingString:sectionType];
	
	headerLabel.text = header;
	headerLabel.font = [Joves_Tools appFontofSize:16];
	headerLabel.textColor = [UIColor whiteColor];
	[headerView addSubview:headerLabel];
	return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
	static NSString *title = @"title";
	static NSString *imgURL = @"image";
	NSString *description = @"description";
	if ([sectionType isEqualToString:@"Agenda"]) {
		description = @"summary";
	}

	
	NSUInteger row = indexPath.row;
	NSDictionary *_cellData = [_data objectAtIndex:indexPath.row];
	NSString *img = [NSString stringWithFormat:@"http://w110.bcn.cat/%@",[_cellData objectForKey:imgURL]];
	
	JovesWhiteCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	cell = [[JovesWhiteCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
	cell.textLabel.text = [_cellData objectForKey:title];
	cell.detailTextLabel.text = [_cellData objectForKey:description];
	cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:img]]];
	/*
	cell.imageView.image = nil;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		
		dispatch_sync(dispatch_get_main_queue(), ^{
			cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:img]]];
		});
    });
	*/
	if (row%2 ==0) {
		cell.contentView.backgroundColor = [UIColor whiteColor];
		return cell;
	}
	
	else
		
	{
		cell.contentView.backgroundColor = [Joves_Tools appLightGrayColor];
		return cell;
	}
	return nil;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
	if ([sectionType isEqualToString:@"Agenda"]) {
		NSDictionary *_cellData = [_data objectAtIndex:indexPath.row];
			
		JovesAgendaDetailViewController *detailViewController = [[JovesAgendaDetailViewController alloc] initWithURL:[_cellData objectForKey:@"link"] withHeader:[_cellData objectForKey:@"title"]];
		[self.navigationController pushViewController:detailViewController animated:YES];
		
	}
	
	else {
		
	NSString* key = [self returntype:sectionType];
	
	NSDictionary *selectedObj = [[JovesAppDelegate sharedAppDelegate].data.cachedData objectForKey:key];
	
	NSString *section = [NSString stringWithFormat:@"section%i",indexPath.row];
		
	id findType = [selectedObj objectForKey:section];
	
	NSDictionary *_cellData = [_data objectAtIndex:indexPath.row];
	
	NSString *derivedHeader = [_cellData objectForKey:@"title"];
	
	
	if ([findType isKindOfClass:[NSArray class]]) {
		NSString *subSectionHeader = [[header stringByAppendingString:@" > "] stringByAppendingString:derivedHeader];
		JovesSectionTableViewController *detailViewController = [[JovesSectionTableViewController alloc] initWithData:findType withHeader:subSectionHeader andType:key];
		[self.navigationController pushViewController:detailViewController animated:YES];
	}
	if ([findType isKindOfClass:[NSString class]]) {
		NSString *subSectionHeader = [[header stringByAppendingString:@" > "] stringByAppendingString:derivedHeader];
		JovesWebSectionViewController *detailViewController = [[JovesWebSectionViewController alloc] initWithData:findType withHeader:subSectionHeader andSection:sectionType];
		[self.navigationController pushViewController:detailViewController animated:YES];
	}
	}
}

-(NSString *)returntype:(NSString *)givetype {
	
	NSString *subsectionstring = [givetype.lowercaseString stringByAppendingString:@"subsections"];
	return subsectionstring;
}

@end
