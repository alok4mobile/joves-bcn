//
//  Joves_Tools.m
//  Joves
//
//  Created by Jan Baros on 11/21/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import "Joves_Tools.h"

@implementation Joves_Tools

+(UIFont *)appFontofSize:(CGFloat)size {
	return [UIFont fontWithName:@"HelveticaNeue" size:size];
}

+(UIFont *)boldappFontofSize:(CGFloat)size {
	return [UIFont fontWithName:@"HelveticaNeue-Bold" size:size];
}

+ (UIColor *) appBlueColor {
	return [UIColor colorWithRed:26.0f/255.0f green:151.0f/255.0f blue:216.0f/255.0f alpha:1];
}

+ (UIColor *) appLightBlueColor {
	return [UIColor colorWithRed:25.0f/255.0f green:151.0f/255.0f blue:216.0f/255.0f alpha:1];
}

+ (UIColor *) appGrayColor {
	return [UIColor colorWithRed:90.0f/255.0f green:89.0f/255.0f blue:89.0f/255.0f alpha:1];
}

+ (UIColor *) appLightGrayColor {
	return [UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:235.0f/255.0f alpha:1];
}

@end
