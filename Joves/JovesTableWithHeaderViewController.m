//
//  JovesTableWithHeaderViewController.m
//  Joves
//
//  Created by Jan Baros on 11/30/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import "JovesTableWithHeaderViewController.h"
#import "JovesWebSectionViewController.h"
#import "JovesSectionTableViewController.h"

@interface JovesTableWithHeaderViewController ()

@end

@interface webCell : UITableViewCell <UIWebViewDelegate>{
	UIWebView *headerContent;
	NSString *link;
	NSString *implementedScript;
}
-(id)initWithWeb:(NSString*)webLink script:(NSString*)script;
@end

@implementation JovesTableWithHeaderViewController

- (id)initWithType:(NSString*)type
{
    self = [super init];
    if (self) {
		header = type;
		NSDictionary *content = [[JovesAppDelegate sharedAppDelegate].data.cachedData objectForKey:header];
		headerData = [content objectForKey:@"header"];
		data = [content objectForKey:@"content"];
		
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self.navigationController setNavigationBarHidden:NO];
	//self.title = @"Cultura";
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	
    // Return the number of rows in the section.
    return data.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    static NSString *title = @"title";
	static NSString *imgURL = @"image";
	static NSString *description = @"description";
	
	NSUInteger row = indexPath.row;
	
	if (row == 0) {
		
		NSString *script = @"document.getElementById('page-content').innerHTML";
		
				
		if ([header isEqualToString:@"Cultura y Ocio"] || [header isEqualToString:@"Medi Ambient i Sostenibilitat"]) {
			
			script = @"document.getElementById('detall-noticia').innerHTML";
		}
		
		
				
		
		
		webCell *cell = [[webCell alloc] initWithWeb:headerData script:script];
		return cell;
		
		
		
	}
	
	else  if (row !=0){
		
		JovesWhiteCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		NSDictionary *_cellData = [data objectAtIndex:row - 1];
		
		cell = [[JovesWhiteCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
		cell.textLabel.text = [_cellData objectForKey:title];
		cell.detailTextLabel.text = [_cellData objectForKey:description];
		NSString *img = [NSString stringWithFormat:@"http://w110.bcn.cat/%@",[_cellData objectForKey:imgURL]];
		cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:img]]];
		
		if (row%2 ==0) {
			cell.contentView.backgroundColor = [UIColor whiteColor];
			return cell;
		}
		
		else
			
		{
			cell.contentView.backgroundColor = [Joves_Tools appLightGrayColor];
			return cell;
		}
		
		
		
		return cell;
		
	}
	return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 40;//was 40
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	
	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 70)];
	headerView.backgroundColor = [Joves_Tools appBlueColor];
	
	UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, headerView.bounds.size.width, 40)];
	headerLabel.backgroundColor = [UIColor clearColor];
	headerLabel.text =  [@"Home > " stringByAppendingString:header];
	headerLabel.font = [Joves_Tools appFontofSize:16];
	headerLabel.textColor = [UIColor whiteColor];
	[headerView addSubview:headerLabel];
	return headerView;
}

#pragma mark - Table view delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSInteger row = indexPath.row;
	
	if (row == 0) {
		return 240;
	}
	else return 100;
	return 0;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSInteger actualRow  = indexPath.row - 1;
	
	NSDictionary *_cellData = [data objectAtIndex:actualRow];
	NSString *derivedHeader = [_cellData objectForKey:@"title"];
	NSString *subSectionHeader = [@"Home > "  stringByAppendingString:[[header stringByAppendingString:@" > "] stringByAppendingString:derivedHeader]];
	
	if (indexPath.row != 0) {
		
		//JovesWebSectionViewController *detailView = [[JovesWebSectionViewController alloc] initWithSection:actualRow andKey:@"culturasubsections" withHeader:subSectionHeader];
		//[self.navigationController pushViewController:detailView animated:YES];
		
		NSString* key = [self returntype:header];
		
		NSDictionary *selectedObj = [[JovesAppDelegate sharedAppDelegate].data.cachedData objectForKey:key];
		
		NSString *section = [NSString stringWithFormat:@"section%i",actualRow];
		
		id findType = [selectedObj objectForKey:section];
		
		NSDictionary *_cellData = [data objectAtIndex:actualRow];
		
		NSString *derivedHeader = [_cellData objectForKey:@"title"];
		
		
		if ([findType isKindOfClass:[NSArray class]]) {
			NSString *subSectionHeader = [[header stringByAppendingString:@" > "] stringByAppendingString:derivedHeader];
			JovesSectionTableViewController *detailViewController = [[JovesSectionTableViewController alloc] initWithData:findType withHeader:subSectionHeader andType:header];
			[self.navigationController pushViewController:detailViewController animated:YES];
		}
		if ([findType isKindOfClass:[NSString class]]) {
			NSString *subSectionHeader = [[header stringByAppendingString:@" > "] stringByAppendingString:derivedHeader];
			JovesWebSectionViewController *detailViewController = [[JovesWebSectionViewController alloc] initWithData:findType withHeader:subSectionHeader andSection:header];
			[self.navigationController pushViewController:detailViewController animated:YES];
		}
		
		
		
	}
}

-(NSString *)returntype:(NSString *)givetype {
	
	NSString *subsectionstring = [givetype.lowercaseString stringByAppendingString:@"subsections"];
	return subsectionstring;
}


@end

@implementation webCell

-(id)initWithWeb:(NSString*)webLink script:(NSString*)script
{
	
	self = [super init];
	if (self)
	{	implementedScript = script;
		link = webLink;
	}
	return self;
}

-(void)layoutSubviews
{
	[super layoutSubviews];
	
	headerContent = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
	[headerContent loadHTMLString:link baseURL:nil];
	headerContent.delegate = self;
	//headerContent.hidden = YES;
	
	if([headerContent respondsToSelector:@selector(scrollView)]){
		[headerContent.scrollView setScrollEnabled:NO];
	}
	
	[self addSubview:headerContent];
	
	self.backgroundColor = [UIColor clearColor];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
	//NSString *script = @"document.getElementById('detall-noticia').innerHTML";
	//	NSString *script = @"document.getElementById('page-content').innerHTML";
    implementedScript = [webView stringByEvaluatingJavaScriptFromString:implementedScript];
	
	if (![implementedScript isEqualToString:@""])
	{
		implementedScript = [[@"<html><head><style> * {font-family: Helvetica;font-size: 13px;} </style></head><body>" stringByAppendingString:implementedScript ] stringByAppendingString:@"</body></html>"];
		[headerContent loadHTMLString:implementedScript baseURL:nil];
		NSLog(@"script <%@>",implementedScript);
		//headerContent.hidden = NO;
	}
	
}

@end