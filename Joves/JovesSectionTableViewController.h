//
//  JovesTreballTableViewController.h
//  Joves
//
//  Created by Jan Baros on 11/26/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JovesSectionTableViewController : UITableViewController {
	int sectionNumber;
	NSArray *subsectionData;
	NSString *_header;
	NSString *sectionType;
}
- (id)initWithData:(NSArray*)data withHeader:(NSString*)header andType:(NSString*)type;

@end
