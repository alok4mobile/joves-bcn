//
//  JovesTreballLevel1ViewController.h
//  Joves
//
//  Created by Jan Baros on 11/21/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JovesWebSectionViewController : UIViewController <UIWebViewDelegate> {
	NSString *_data;
	UIWebView *viewWeb;
	NSString *_header;
	NSString *sectionType;
	BOOL pageAlreadyFav;
	NSMutableDictionary *favData;
}

- (id)initWithData:(NSString*)data withHeader:(NSString*)header andSection:(NSString*)section;

@end
