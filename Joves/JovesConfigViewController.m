//
//  JovesConfigViewController.m
//  Joves
//
//  Created by Alok Sahay on 11/19/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import "JovesConfigViewController.h"
#import "JovesAboutGrupoADIViewController.h"

@interface JovesConfigViewController ()

@end

@implementation JovesConfigViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Config", nil);
        self.tabBarItem.image = [UIImage imageNamed:@"settings_inactive"];    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	_settingsTable = [[UITableView alloc]initWithFrame:CGRectMake(20, 40, self.view.frame.size.width-40, 280) style:UITableViewStyleGrouped];
	_settingsTable.backgroundColor = [UIColor clearColor];
	_settingsTable.backgroundView = nil;
	_settingsTable.dataSource= self;
	_settingsTable.delegate = self;
	_settingsTable.scrollEnabled = NO;
	_settingsTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
	[self.view addSubview:_settingsTable];
	
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	 return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case(0):
            return @"Idiomes";
            break;
        default:
           break;
	}
    return nil;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *CellIdentifier = [ NSString stringWithFormat: @"%d:%d", [ indexPath indexAtPosition: 0 ], [ indexPath indexAtPosition:1 ]];
	NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
	UITableViewCell *cell = [ tableView dequeueReusableCellWithIdentifier: CellIdentifier];
	
	if (cell == nil) {
		cell = [[ UITableViewCell alloc ] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: CellIdentifier];
		
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.textLabel.textColor = [UIColor colorWithRed:53.0f/255.0f green:170.0f/255.0f blue:225.0f/255.0f alpha:1.0f];
		switch ([ indexPath indexAtPosition: 0]) {
			case(0):
				
				switch([ indexPath indexAtPosition: 1]) {
						
					case(0):
					{
						if ([language isEqualToString:@"ca"]) {
							cell.accessoryType = UITableViewCellAccessoryCheckmark;
						}
						cell.textLabel.text = @"Català"; // OS3
					}
						break;
						
					case(1):
					{
						if (![language isEqualToString:@"ca"]) {
							cell.accessoryType = UITableViewCellAccessoryCheckmark;
						}
						cell.textLabel.text = @"Castellà"; // OS3
					}
						break;
						
				}
				
				break;
				
			default:
								
				break;
				
		}
	}
	
	return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	switch ([ indexPath indexAtPosition: 0]) {
			
		case(0):
			
			switch([ indexPath indexAtPosition: 1]) {
					
				case(0):
				{
					
					UITableViewCell *cell1 = [tableView cellForRowAtIndexPath:indexPath];
					NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:1 inSection:indexPath.section];
					
					UITableViewCell *cell2 = [tableView cellForRowAtIndexPath:indexPath1];
					if (cell1.selected) {
						cell1.accessoryType = UITableViewCellAccessoryCheckmark;
						cell2.accessoryType = UITableViewCellAccessoryNone;
					}
					[[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"ca", nil] forKey:@"AppleLanguages"]; //switching to Catalan locale
					[[NSUserDefaults standardUserDefaults] synchronize];
					[[JovesAppDelegate sharedAppDelegate] reloadData];
				}
					break;
					
				case(1):
				{
					
					UITableViewCell *cell2 = [tableView cellForRowAtIndexPath:indexPath];
					NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:0 inSection:indexPath.section];
					UITableViewCell *cell1 = [tableView cellForRowAtIndexPath:indexPath1];
					if (cell2.selected) {
						cell2.accessoryType = UITableViewCellAccessoryCheckmark;
						cell1.accessoryType = UITableViewCellAccessoryNone;
					}
					[[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"es", nil] forKey:@"AppleLanguages"]; //switching to Spanish locale
					[[NSUserDefaults standardUserDefaults] synchronize];
					[[JovesAppDelegate sharedAppDelegate] reloadData];
				}
					break;
					
			}
			
			break;
			
		case(1):
			
			switch ([ indexPath indexAtPosition: 1 ]) {
					
				case(0):
				{
					JovesAboutGrupoADIViewController *aboutUs = [[JovesAboutGrupoADIViewController alloc] init];
					[self.navigationController pushViewController:aboutUs animated:YES];
				}
					break;
			}
			break;
	}
}

-(void)viewWillAppear:(BOOL)animated {
	[_settingsTable deselectRowAtIndexPath:[_settingsTable indexPathForSelectedRow] animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
