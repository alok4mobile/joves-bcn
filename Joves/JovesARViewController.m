//
//  JovesARViewController.m
//  Joves
//
//  Created by Jan Baros on 12/20/12.
//  Copyright (c) 2012 groupoadi. All rights reserved.
//

#import "JovesARViewController.h"

@interface JovesARViewController () {
	UIImageView *visor;
}

@property (nonatomic, strong) ArgolView *gpsView;

@end

@interface ARtableCell : UITableViewCell {
	UITableViewCellStyle cellstyle;
}
@end

@interface MapViewAnnotation : NSObject <MKAnnotation> {
	NSString *title;
	CLLocationCoordinate2D coordinate;
}

@property (nonatomic,copy) NSString *title;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

-(id)initWithTitle:(NSString*)ttl andcood:(CLLocationCoordinate2D)c2d;

@end

@implementation JovesARViewController

@synthesize gpsView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _data = [JovesAppDelegate sharedAppDelegate].targetPOI;
		selectedCase = 0;
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(targetsUpdated) name:@"4mAR_TARGETS_CHECKED" object:nil];
		
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self.navigationController setNavigationBarHidden:NO];
	
	
	
	UIImage *map_on;
	UIImage *radar_on;
	
	
	UIImage *map_off;
	UIImage *radar_off;
	
	NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
	if ([language isEqualToString:@"ca"])
	{
		map_off = [UIImage imageNamed:@"04-map-button_CAT_OFF"];
		radar_off = [UIImage imageNamed:@"02-radar-button_CAT_OFF"];
		
		
		map_on = [UIImage imageNamed:@"04-map-button_CAT_ON"];
		radar_on = [UIImage imageNamed:@"02-radar-button_CAT_ON"];
	}
	else if (![language isEqualToString:@"ca"])
	{
		
		
		map_off = [UIImage imageNamed:@"04-map-button_ESP_OFF"];
		radar_off = [UIImage imageNamed:@"02-radar-button_ESP_OFF"];
		
		map_on = [UIImage imageNamed:@"04-map-button_ESP_ON"];
		radar_on = [UIImage imageNamed:@"02-radar-button_ESP_ON"];
	}
	
	
	CGRect frame = self.view.frame;
	frame.origin.y -=40;
	
	self.gpsView = [[ArgolView alloc] initWithFrame:frame];
	
	CGFloat buttonSize = 50;
	CGFloat buttonStart = 350;
	CGFloat menuButtonOffset = 60;
	
	
	UIButton *radarButton = [[UIButton alloc] initWithFrame:CGRectMake(45, buttonStart, buttonSize,buttonSize)];
	[radarButton setImage:radar_off forState:UIControlStateNormal];
	[radarButton setImage:radar_on forState:UIControlStateSelected];
	[radarButton addTarget:self action:@selector(selectedMenu:) forControlEvents:UIControlEventTouchUpInside];
	radarButton.selected = YES;
	
	
	UIButton *mapButton = [[UIButton alloc] initWithFrame:CGRectMake(radarButton.frame.origin.x + menuButtonOffset, buttonStart, buttonSize, buttonSize)];
	[mapButton setImage:map_off forState:UIControlStateNormal];
	[mapButton setImage:map_on forState:UIControlStateSelected];
	[mapButton addTarget:self action:@selector(selectedMenu:) forControlEvents:UIControlEventTouchUpInside];
	
	arbuttons = [[NSArray alloc] initWithObjects:radarButton,mapButton, nil];
	
	CGFloat radaroffsetx = 40;
	CGFloat radaroffsety = 75;
	
	radarView = [[UIView alloc] initWithFrame:CGRectMake(radaroffsetx, radaroffsety, self.view.frame.size.width - (2*radaroffsetx), self.view.frame.size.width - (2*radaroffsetx))];
	radarView.backgroundColor = [UIColor clearColor];
	
	poiView = [[UIView alloc] initWithFrame:radarView.frame];
	poiView.backgroundColor = [UIColor clearColor];
	
	visor = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, radarView.frame.size.width, radarView.frame.size.height)];
	visor.image = [UIImage imageNamed:@"RA-visor"];
	
	CGFloat rangeoffsetx = 47;
	CGFloat rangebottom = 90;
	
	UIImageView *whiteRange = [[UIImageView alloc] initWithFrame:CGRectMake(rangeoffsetx, 9, visor.frame.size.width - (2*rangeoffsetx) - 7, rangebottom)];
	whiteRange.image = [UIImage imageNamed:@"RA-white range"];
	
	[radarView addSubview:visor];
	[radarView addSubview:whiteRange];
	
	CGFloat tablestartx = 30;
	CGFloat tablestarty = 55;
	
	poiList = [[UITableView alloc] initWithFrame:CGRectMake(tablestartx, tablestarty, self.view.frame.size.width - (2*tablestartx), buttonStart - tablestarty - 5) style:UITableViewStylePlain];
	poiList.backgroundColor = [UIColor colorWithWhite:1.0f alpha:0.8];
	poiList.hidden = YES;
	poiList.rowHeight = 30;
	poiList.delegate = self;
	poiList.dataSource = self;
	
	flipList = [[UITableView alloc] initWithFrame:CGRectMake(tablestartx, tablestarty, self.view.frame.size.width - (2*tablestartx), buttonStart - tablestarty - 5) style:UITableViewStylePlain];
	flipList.backgroundColor = [UIColor colorWithWhite:1.0f alpha:0.8];
	flipList.hidden = YES;
	flipList.rowHeight = 30;
	flipList.delegate = self;
	flipList.dataSource = self;
	
	
	tematicButton = [[UIButton alloc] initWithFrame:CGRectMake(radarButton.frame.origin.x + menuButtonOffset + 120, buttonStart, buttonSize, buttonSize)];
	[tematicButton setImage:[UIImage imageNamed:@"03-TEMATIC-button_CAT_OFF"] forState:UIControlStateNormal];
	[tematicButton setImage:[UIImage imageNamed:@"03-TEMATIC-button_CAT_ON"] forState:UIControlStateSelected];
	tematicButton.tag = 2;
	[tematicButton addTarget:self action:@selector(selectedTable:) forControlEvents:UIControlEventTouchUpInside];
	
	districtsButton = [[UIButton alloc] initWithFrame:CGRectMake(radarButton.frame.origin.x + menuButtonOffset + 60, buttonStart, buttonSize, buttonSize)];
	[districtsButton setImage:[UIImage imageNamed:@"03-DISTRICT-button_CAT_OFF"] forState:UIControlStateNormal];
	[districtsButton setImage:[UIImage imageNamed:@"03-DISTRICT-button_CAT_ON"] forState:UIControlStateSelected];
	districtsButton.tag = 1;
	[districtsButton addTarget:self action:@selector(selectedTable:) forControlEvents:UIControlEventTouchUpInside];
	
#pragma mark Other items array
	
	
	poiMap = [[MKMapView alloc] initWithFrame:frame];
	poiMap.delegate = self;
	poiMap.hidden = YES;
	flipList.hidden = YES;
	
	[self.view addSubview:self.gpsView];
	[self.view addSubview:radarView];
	[self.view addSubview:poiMap];
	[self.view addSubview:poiList];
	[self.view addSubview:flipList];
	[self.view addSubview:poiView];
	[self.view addSubview:tematicButton];
	[self.view addSubview:districtsButton];
	
	
	[self.view addSubview:radarButton];
	[self.view addSubview:mapButton];
	
	NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"target" ascending:YES selector:@selector(compareDistance:)];
	
	NSArray *sortedTargets = [[JovesAppDelegate sharedAppDelegate].targetPOI sortedArrayUsingDescriptors:@[sortDescriptor]];
	
	//POIsOnMap = [sortedTargets copy];
	NSRange limitPOIs;
	limitPOIs.location = 0;
	
	if (sortedTargets.count>7) {
		limitPOIs.length = 7;
	}
	else limitPOIs.length = sortedTargets.count;
	
	NSArray *subFilteredTargets = [sortedTargets subarrayWithRange:limitPOIs];
	POIsOnMap = [subFilteredTargets copy];
	
}

#pragma mark bottom buttons

-(void)selectedMenu:(UIButton*)pressedButton
{
	for (UIButton *selectedButton in arbuttons) {
		selectedButton.selected = NO;
	}
	
	int index = [arbuttons indexOfObject:pressedButton];
	
	if (index == 1) {
		// MAP
		poiList.hidden = YES;
		flipList.hidden = YES;
		radarView.hidden = YES;
		poiMap.hidden = NO;
		districtsButton.selected = NO;
		tematicButton.selected = NO;
		[poiList reloadData];
		[self selectedMap];
	}
	else if (index == 0){
		//RADAR view
		radarView.hidden = NO;
		poiList.hidden = YES;
		poiMap.hidden = YES;
		districtsButton.selected = NO;
		tematicButton.selected = NO;
		flipList.hidden = YES;
		[self selectedAR];
	}
	pressedButton.selected = YES;
}

-(void)selectedAR {
	
	NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"target" ascending:YES selector:@selector(compareDistance:)];
	
	NSArray *sortedTargets = [[JovesAppDelegate sharedAppDelegate].targetPOI sortedArrayUsingDescriptors:@[sortDescriptor]];
	
	NSRange tenObjects;
	tenObjects.location = 0;
	
	if (sortedTargets.count>10) {
		tenObjects.length = 10;
	}
	else tenObjects.length = sortedTargets.count;
	
	NSArray *subFilteredTargets = [sortedTargets subarrayWithRange:tenObjects];
	poiView.hidden = NO;
	
}


-(void)selectedMap {
	
	NSMutableArray *annotationsToRemove = [poiMap.annotations mutableCopy] ;
	[poiMap removeAnnotations:annotationsToRemove];
	
	poiView.hidden = YES;
	poiList.hidden = YES;
	radarView.hidden = YES;
	poiMap.hidden = NO;
	
	if (!POIsOnMap) {
		
		NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"target" ascending:YES selector:@selector(compareDistance:)];
		
		NSArray *sortedTargets = [[JovesAppDelegate sharedAppDelegate].targetPOI sortedArrayUsingDescriptors:@[sortDescriptor]];
		
		NSRange tenObjects;
		tenObjects.location = 0;
		
		if (sortedTargets.count>10) {
			tenObjects.length = 10;
		}
		else tenObjects.length = sortedTargets.count;
		
		NSArray *subFilteredTargets = [sortedTargets subarrayWithRange:tenObjects];
		
		
		
		for (Joves_target *poi in subFilteredTargets ) {
			
			CLLocationCoordinate2D location;
			location.latitude = [poi.poi.latitude doubleValue];
			location.longitude = [poi.poi.longitude doubleValue];
			
			MapViewAnnotation *annotation = [[MapViewAnnotation alloc] initWithTitle:poi.poi.name andcood:location];
			[poiMap addAnnotation:annotation];
			
		}
		
		Joves_target *closest = [subFilteredTargets objectAtIndex:0];
		
		
		CLLocationCoordinate2D zoomLocation;
		zoomLocation.latitude = closest.poi.latitude.doubleValue;
		zoomLocation.longitude = closest.poi.longitude.doubleValue;
		
		MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation,6.0*METERS_PER_MILE, 6.0*METERS_PER_MILE);
		MKCoordinateRegion adjustedRegion = [poiMap regionThatFits:viewRegion];
		
		[poiMap setRegion:adjustedRegion animated:YES];
	}
	
	else {
		for (Joves_target *poi in POIsOnMap ) {
			
			CLLocationCoordinate2D location;
			location.latitude = [poi.poi.latitude doubleValue];
			location.longitude = [poi.poi.longitude doubleValue];
			
			MapViewAnnotation *annotation = [[MapViewAnnotation alloc] initWithTitle:poi.poi.name andcood:location];
			[poiMap addAnnotation:annotation];
			
		}
		
		Joves_target *closest = [POIsOnMap objectAtIndex:0];
		
		
		CLLocationCoordinate2D zoomLocation;
		zoomLocation.latitude = closest.poi.latitude.doubleValue;
		zoomLocation.longitude = closest.poi.longitude.doubleValue;
		
		MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation,6.0*METERS_PER_MILE, 6.0*METERS_PER_MILE);
		MKCoordinateRegion adjustedRegion = [poiMap regionThatFits:viewRegion];
		
		[poiMap setRegion:adjustedRegion animated:YES];
		
	}
	
	
}

#pragma mark AR pressed

-(void)selectedTable:(UIButton*)pressedButton {
	
	
	for (UIButton *selectedButton in arbuttons) {
		selectedButton.selected = NO;
	}
	radarView.hidden = YES;
	
	poiMap.hidden = YES;
	poiView.hidden = YES;
	
	if (pressedButton.tag == 1) {
		tematicButton.selected = NO;
		districtsButton.selected = YES;
		_tableData = nil;
		_tableData = [NSArray new];
		currentRow = nil;
		filteredItems = nil;
		_tableData = [self.areas sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		flipList.hidden = YES;
		categoryNotArea = NO;
		poiList.hidden = NO;
	}
	
	else if (pressedButton.tag == 2) {
		
		tematicButton.selected = YES;
		districtsButton.selected = NO;
		_tableData = nil;
		_tableData = [NSArray new];
		currentRow = nil;
		filteredItems = nil;
		_tableData = [self.categories sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		flipList.hidden = YES;
		categoryNotArea = YES;
		poiList.hidden = NO;
	}
	
	[poiList reloadData];
	
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	if (tableView == poiList) {
		return _tableData.count;
	}
	
	else if (tableView == flipList) {
		return flipData.count+1;
	}
	return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (tableView == poiList) {
		
		int selectedRow = indexPath.row;
		
		NSString *cellIdentifier = @"Cell Identifier";
		
		ARtableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
		
		NSString *poi = [_tableData objectAtIndex:selectedRow];
		
		cell = [[ARtableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
		
		cell.textLabel.text = poi;
		
		//cell.contentView.backgroundColor = [UIColor clearColor];
		//cell.backgroundColor = [UIColor clearColor];
		
		
		//NSString *distance = [NSString stringWithFormat:@"%i m",(int)poi.target.distance];
		//cell.detailTextLabel.text = distance;
		
		
		if (filteredItems.count!=0) {
			
			if (selectedRow > rowRange.location && selectedRow <rowRange.location +rowRange.length+1 )
			{
				cell.contentView.backgroundColor = [UIColor whiteColor];
				return cell;
			}
			
		}
		
		
		return cell;
	}
	
	else if (tableView == flipList) {
		
		if (indexPath.row == flipData.count) {
			NSString *cellIdentifier = @"Cell Identifier";
			
			ARtableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
			
			cell = [[ARtableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
			cell.textLabel.text = @"Show All"; // later change to Spanish/Catalan
			
			return cell;
		}
		else {
			
			int selectedRow = indexPath.row;
			
			NSString *cellIdentifier = @"Cell Identifier";
			
			ARtableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
			
			Joves_target *target = [flipData objectAtIndex:selectedRow];
			
			cell = [[ARtableCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
			cell.textLabel.text = target.poi.name;
			cell.detailTextLabel.text = [NSString stringWithFormat:@"%i m",(int)target.target.distance];
			return cell;
		}
	}
	return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	
	int selectedRow = indexPath.row;
	
	if (tableView == poiList) {
		/*
		 
		 case 0: original table, press a cell and a drop down appears.
		 case 1: press the same cell again, it closes.
		 case 2: press some other main cell, the original one closes, new one drops.
		 case 3: press the sub menu, view flips.
	 	 
		 */
		NSLog(@"Selected row: %i",selectedRow);
		
		if (filteredItems.count == 0) {
			selectedCase = 0;
		}
		
		else if ([[_tableData objectAtIndex:indexPath.row] isEqualToString:currentRow] && filteredItems.count!=0) { // case 1
			selectedCase = 1;
		}
		
		else if ((![[_tableData objectAtIndex:indexPath.row] isEqualToString:currentRow] && filteredItems.count!=0) && ![filteredItems containsObject:[_tableData objectAtIndex:indexPath.row]]) //case 2
		{
			selectedCase = 2;
		}
		
		else if ([filteredItems containsObject:[_tableData objectAtIndex:indexPath.row]]) //case 3
		{
			selectedCase = 3;
		}
		
		
		NSLog(@"case: %i",selectedCase);
		
		
		switch (selectedCase) {
			case 0: //original table, press a cell and a drop down appears.
			{
				
				NSString *selected = [_tableData objectAtIndex:indexPath.row];
				
				if (categoryNotArea == YES) {
					
					filteredItems = [[self AreaWithCategory:selected] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
					//[self.categories sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
				}
				
				else if (categoryNotArea == NO) {
					filteredItems = [[self CategoryWithArea:selected]sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
				}
				
				NSMutableArray *moddedTableData = [NSMutableArray new];
				
				NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(indexPath.row+1, filteredItems.count)];
				
				moddedTableData =[_tableData mutableCopy];
				[moddedTableData insertObjects:filteredItems atIndexes:indexSet];
				
				_tableData = [moddedTableData copy];
				
				NSMutableArray *indexes = [NSMutableArray new];
				
				for (int j = 0; j< filteredItems.count; ++j) {
					
					NSIndexPath *index = [NSIndexPath indexPathForRow:indexPath.row+j+1 inSection:0];
					[indexes addObject:index];
				}
				
				[tableView beginUpdates];
				[tableView insertRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationBottom];
				
				[tableView endUpdates];
				
				rowRange.location = selectedRow;
				rowRange.length = filteredItems.count;
				currentRow = selected;
				
			}
				break;
				
			case 1: //cell closes
			{
				
				NSString *selected = [_tableData objectAtIndex:indexPath.row];
				NSLog(@"Selected cell: %@",selected);
				
				if (categoryNotArea == YES) {
					
					filteredItems = [[self AreaWithCategory:selected] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
					//[self.categories sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
				}
				
				else if (categoryNotArea == NO) {
					filteredItems = [[self CategoryWithArea:selected]sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
				}
				
				NSMutableArray *moddedTableData = [NSMutableArray new];
				
				moddedTableData =[_tableData mutableCopy];
				[moddedTableData removeObjectsInArray:filteredItems];
				
				_tableData = [moddedTableData copy];
				
				NSMutableArray *indexes = [NSMutableArray new];
				
				for (int j = 0; j< filteredItems.count; ++j) {
					
					NSIndexPath *index = [NSIndexPath indexPathForRow:indexPath.row+j+1 inSection:0];
					[indexes addObject:index];
				}
				
				[tableView beginUpdates];
				[tableView deleteRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationBottom];
				[tableView endUpdates];
				
				selectedCase = 0;
				filteredItems = nil;
				
			}
				
				break;
				
			case 2: // some other main cell pressed so delete previous cells
			{
				
				//Delete original rows
				int originalindex = [_tableData indexOfObject:currentRow];
				
				NSString *selected = currentRow;
				NSLog(@"Deleted cell: %@",selected);
				
				if (categoryNotArea == YES) {
					
					filteredItems = [self AreaWithCategory:selected] ;
					//[self.categories sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
				}
				
				else if (categoryNotArea == NO) {
					filteredItems = [self CategoryWithArea:selected];
				}
				
				NSMutableArray *moddedTableData = [NSMutableArray new];
				
				moddedTableData =[_tableData mutableCopy];
				[moddedTableData removeObjectsInArray:filteredItems];
				
				_tableData = [moddedTableData copy];
				
				NSMutableArray *indexes = [NSMutableArray new];
				
				for (int j = 0; j< filteredItems.count; ++j) {
					
					NSIndexPath *index = [NSIndexPath indexPathForRow:originalindex+j+1 inSection:0];
					[indexes addObject:index];
				}
				
				//int minus = filteredItems.count;
				
				[tableView beginUpdates];
				[tableView deleteRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationBottom];
				[tableView endUpdates];
				filteredItems = nil;
				
			}
				
				break;
				
			case 3: // subsection selected.
			{
				NSLog(@"Flip view");
				
				NSString *category;
				NSString *area;
				
				
				if (categoryNotArea == YES) {
					
					category = currentRow;
					area = [_tableData objectAtIndex:indexPath.row];
				}
				
				else if (categoryNotArea == NO) {
					area = currentRow;
					category = [_tableData objectAtIndex:indexPath.row];
				}
				
				NSMutableArray *filteredPOIs = [NSMutableArray new];
				
				NSLog(@"category = '%@' and area = '%@'",category,area);
				
				
				for (Joves_target *target in _data) {
					if ([target.poi.area isEqualToString:area] && [target.poi.category isEqualToString:category]) {
						[filteredPOIs addObject:target];
					}
				}
				
				flipData = [filteredPOIs copy];
				[flipList reloadData];
				poiList.hidden = YES;
				flipList.hidden = NO;
			}
				
				
				
			default:
				break;
		}
		
		[poiList reloadData];
		
	}
	
	else if (tableView == flipList) {
		
		POIsOnMap = [NSMutableArray new];
		
		if (selectedRow == flipData.count) { // show all POIs
			
			POIsOnMap = [flipData copy];
			poiList.hidden = YES;
			flipList.hidden = YES;
			[self selectedMap];
			
		}
		
		else { // show selectedPOI
			poiList.hidden = YES;
			flipList.hidden = YES;
			selectedPOI = [flipData objectAtIndex:indexPath.row];
			[POIsOnMap removeAllObjects];
			[POIsOnMap addObject:selectedPOI];
			[self selectedMap];
		}
		
		//poiView.hidden = NO;
	}
	
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(MapViewAnnotation<MKAnnotation>*)annotation {
	
	MKPinAnnotationView *annotationView = (MKPinAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier:nil];
	annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:nil];
	
	annotationView.enabled = YES;
	annotationView.canShowCallout = YES;
	annotationView.animatesDrop = YES;
	/*annotationView.image = [UIImage imageNamed:@"RA-POI-icon"]; // change image when you have one.
	 
	 UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
	 
	 NSLog(@"POI Name: %@",annotation.title);
	 [rightButton setTitle:annotation.title forState:UIControlStateNormal];
	 rightButton.titleLabel.text = annotation.title;
	 //[rightButton addTarget:self action:@selector(showDetailEvent:) forControlEvents:UIControlEventTouchUpInside];
	 annotationView.rightCalloutAccessoryView = rightButton;
	 */
	return annotationView;
}

/*
 -(void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
 MKAnnotationView *annotationView = [views objectAtIndex:0];
 id <MKAnnotation> mp = [annotationView annotation];
 MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([mp coordinate], 1500, 1500);
 [mapView setRegion:region animated:YES];
 [mapView selectAnnotation:mp animated:YES];
 }
 */

-(void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:YES];
	[[JovesAppDelegate sharedAppDelegate] stopGPS];
}
#define distancefilter 800
- (void) targetsUpdated {
	
	for (UIView __strong *remove in poiView.subviews) {
		[remove removeFromSuperview];
		remove = nil;
	}
	
	dispatch_sync(dispatch_get_main_queue(), ^{
		
		for (Joves_target *addtarget in POIsOnMap) {
			
			double poiX;
			double poiY;
			double distance;
			
			CGFloat startPointX = visor.frame.origin.x + (visor.frame.size.width/2);
			CGFloat startPointY = visor.frame.origin.y + (visor.frame.size.height/2);
			
			distance = addtarget.target.distance;
			double trueDirection = (addtarget.target.azimuth - addtarget.target.device.currHeading.trueHeading)*0.01745; // degrees to radians
			
			UIButton *poi = [[UIButton alloc] init];
			poi.tag = addtarget.target.tag;
			
			if ([addtarget.poi.category isEqualToString:@"Wi-fi"]) {
				[poi setImage:[UIImage imageNamed:@"icon-WIFI"] forState:UIControlStateNormal];
			}
			if ([addtarget.poi.category isEqualToString:@"Biblioteques"]) {
				[poi setImage:[UIImage imageNamed:@"LIBRARY_icon"] forState:UIControlStateNormal];
			}
			if ([addtarget.poi.category isEqualToString:@"Centre d'Assessorament Acadèmic per a Joves "]) {
				[poi setImage:[UIImage imageNamed:@"GUIDANCE_icon"] forState:UIControlStateNormal];
			}
			if ([addtarget.poi.category isEqualToString:@"Casals"]) {
				[poi setImage:[UIImage imageNamed:@"CASALS_icon"] forState:UIControlStateNormal];
			}
			if ([addtarget.poi.category isEqualToString:@"Espais Joves"]) {
				[poi setImage:[UIImage imageNamed:@"YOUNG_icon"] forState:UIControlStateNormal];
			}
			if ([addtarget.poi.category isEqualToString:@"Instal.lacions esportives"]) {
				[poi setImage:[UIImage imageNamed:@"Sports_cion"] forState:UIControlStateNormal];
			}
			if ([addtarget.poi.category isEqualToString:@"Punts d'assesorament laboral"]) {
				[poi setImage:[UIImage imageNamed:@"Work_icon"] forState:UIControlStateNormal];
			}
			if ([addtarget.poi.category isEqualToString:@"Sala d'estudi nocturna"]) {
				[poi setImage:[UIImage imageNamed:@"Night_study_icon"] forState:UIControlStateNormal];
			}
			if ([addtarget.poi.category isEqualToString:@"Xarxa de Punts d'Informació"]) {
				[poi setImage:[UIImage imageNamed:@"info_icon"] forState:UIControlStateNormal];
			}
			
			poi.backgroundColor = [UIColor clearColor];
			
			
			//[poi addTarget:blockSelf action:@selector(showPOILabel:) forControlEvents:UIControlEventTouchUpInside];
			//NSLog(@"added target %i",addtarget.tag);
			
			if (distance > distancefilter) {
				distance = distancefilter;
			}
			
			poiX = (distance*120*sin(trueDirection))/distancefilter;
			poiY = (distance*120*cos(trueDirection))/distancefilter;
			
			poi.frame =	CGRectMake(startPointX + poiX - 15,startPointY - poiY - 15, 30, 30);
			[poiView addSubview:poi];
		}
				
	});
	
}

-(NSArray *)areas {
	NSMutableSet *areas = [NSMutableSet new];
	for (Joves_target *target in _data)
	{
		[areas addObject:target.poi.area];
	}
	
	return [areas allObjects];
}


-(NSArray *)categories {
	NSMutableSet *category = [NSMutableSet new];
	for (Joves_target *target in _data)
	{
		[category addObject:target.poi.category];
	}
	
	return [category allObjects];
}


- (NSArray *)AreaWithCategory:(NSString *)category {
	
	NSMutableSet *areas = [NSMutableSet new];
	for (Joves_target *target in _data)
	{
		if (category == nil || [target.poi.category isEqualToString:category]) {
			[areas addObject:target.poi.area];
		}
		
	}
	return [areas allObjects];
}

- (NSArray *)CategoryWithArea:(NSString *)area {
	
	NSMutableSet *areas = [NSMutableSet new];
	for (Joves_target *target in _data)
	{
		if (area == nil || [target.poi.area isEqualToString:area]) {
			[areas addObject:target.poi.category];
		}
		
	}
	
	return [areas allObjects];
}

@end

@implementation ARtableCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self)
	{
		self.selectionStyle = UITableViewCellSelectionStyleGray;
		self.textLabel.backgroundColor = [UIColor clearColor];
		self.textLabel.textColor = [UIColor blackColor];
		self.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
		self.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
		cellstyle = style;
	}
	return self;
	
}

-(void)layoutSubviews
{
	[super layoutSubviews];
	//self.contentView.backgroundColor = [UIColor clearColor];
	self.backgroundColor = [UIColor clearColor];
	CGRect textLabelFrame = self.textLabel.frame;
	
	if (cellstyle == UITableViewCellStyleDefault) {
		textLabelFrame.size.width = 240;
	}
	else  {
		textLabelFrame.size.width = 180;
		CGRect detailLabelFrame = self.detailTextLabel.frame;
		detailLabelFrame.size.width = 70;
		detailLabelFrame.origin.x = 190;
		textLabelFrame.origin.y = 8;
		detailLabelFrame.origin.y = 8;
		self.detailTextLabel.frame = detailLabelFrame;
	}
	
	self.textLabel.frame = textLabelFrame;
	
}

@end

@implementation MapViewAnnotation

@synthesize title, coordinate;

-(id)initWithTitle:(NSString *)ttl andcood:(CLLocationCoordinate2D)c2d {
	self = [super init];
	title = ttl;
	coordinate = c2d;
	return self;
}
@end
